import { Injectable } from '@angular/core';

import { LoadingController, Loading } from 'ionic-angular';

/*
  Generated class for the LoaderUtil provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LoaderUtil {
 
  loading: Loading;
  constructor( public loadingCtrl: LoadingController) {
    console.log('Hello LoaderUtil Provider');

    this.loading = null;
    this.hideLoader();

  }
  /**
   * showLoader
   */
  public showLoader() {
    console.log('Hello LoaderUtil Provider1');
    this.hideLoader();
    // console.log('Hello LoaderUtil Provider2');
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: false,
    });
    // console.log('Hello LoaderUtil Provider3');
    this.loading.present();
  }
  /**
   * hideLoader
   */
  public hideLoader() {
    console.log('Hello LoaderUtil Provider4');
    if (this.loading != null) {
      // console.log('Hello LoaderUtil Provider5');
      this.loading.dismiss();
      this.loading = null;
    }
  }

  /**
   * showLoaderWithText
   */
  public showLoaderWithText(text) {
     this.hideLoader();
    // console.log('Hello LoaderUtil Provider2');
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: false,
      content:text
    });
    // console.log('Hello LoaderUtil Provider3');
    this.loading.present();
  }




 
}
