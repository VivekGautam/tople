import { Injectable } from '@angular/core';

import { AlertController, Alert, ActionSheetController } from 'ionic-angular';

/*
  Generated class for the AlertUtil provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AlertUtil {
  currentAlert: Alert;
  testRadioOpen: boolean;
  testRadioResult: any;
  constructor(public alertCtrl: AlertController, public actionSheetCtrl: ActionSheetController) {
    console.log('Hello AlertUtil Provider');
  }
  /**
   * showOkAlert
   */
  public showOkAlertWithDismiss(title: string, body: string, clearPrevious: boolean) {
    if (this.currentAlert != null && clearPrevious) {

      this.currentAlert.dismiss().then(() => {
        this.currentAlert = null;

        this.currentAlert = this.alertCtrl.create({
          title: title,
          subTitle: body,
          buttons: ["OK"]
        });
        this.currentAlert.present();
      });
    } else {
      this.currentAlert = null;
      this.currentAlert = this.alertCtrl.create({
        title: title,
        subTitle: body,
        buttons: ["OK"]
      });
      this.currentAlert.present();
    }

  }
  /**
   * showOkAlert
   */
  public showOkAlert(title: string, body: string) {
    this.currentAlert = null;
    this.currentAlert = this.alertCtrl.create({
      title: title,
      subTitle: body,
      buttons: ["OK"]
    });
    this.currentAlert.present();
  }
    

  public data(){
    return new Promise<any>((resolve) => {
      let alert = this.alertCtrl.create();
      alert.setTitle('Select Package');
      
    })
  }


  public alertData(data, key, type) {
    //type is either radio or checkbox
    return new Promise<any>((resolve) => {
   console.log("enter in data");
   console.log(JSON.stringify(data))
      let alert = this.alertCtrl.create();
      alert.setTitle('Select a Package');
      for (let i = 0; i < data.length; i++) {
        let uppercases: string;
        // this is for object keys checking
        if (key != 0) {
          uppercases = data[i][key];
        }
        else {
          uppercases = data[i]
        }
        alert.addInput({
          type: type,
          label: uppercases.toUpperCase(),
          value: data[i],
          checked: true,
          disabled:true
        });
      }
   
      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: data => {
          this.testRadioOpen = false;
          this.testRadioResult = data;
          resolve(this.testRadioResult);
          console.log("result");
          console.log(this.testRadioResult)

        }
      });
      alert.present();

    })
  }

  actionSheetMtd() {
    return new Promise<string>((resolve) => {
      let selected: string;
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Choose the Photo',
        buttons: [
          {
            text: 'Camera',
            role: 'destructive',
            handler: () => {
              selected = 'Destructive clicked'
              resolve('Camera')
            }
          }, {
            text: 'Gallery',
            handler: () => {
              resolve('Gallery')

            }
          }, {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              resolve('Cancel')

            }
          }
        ]

      });

      actionSheet.present();
    })
  }


// sortObjectByMultipleCriteria(data,criteria)
// {
//   return new Promise<any>((resolve) => {
//     // console.log("sort data " + JSON.stringify(data))
//     data.sort((a, b) => a[[criteria[1]criteria[2]].localeCompare(b[type]))
//     resolve(data.sort((a, b) => a[type].localeCompare(b[type])));
//   })
// }
  sortObject(data, type) {
    return new Promise<any>((resolve) => {
      console.log("sort data " + JSON.stringify(data))
      resolve(data.sort((a, b) => a[type].localeCompare(b[type])));
    })
  }
}
