import { Injectable } from '@angular/core';


 let BASE_URL = " http://cakepotter.com/caterer/posapp/"


//export let BASE_URL = "http://toplecaterers.webfillssoftwares.com/sales/posapp/"
export let  SIGN_IN = BASE_URL + "secureauthuser"
export let GET_CUSTOMERS = BASE_URL + "customers"
export let SAVE_CUSTOMER = BASE_URL + "customeradd"
export let GET_CATEGORIES = BASE_URL + "catalogcategory"
export let GET_CATEGORIES_ITEMS = BASE_URL + "category_items"
export let GET_PACKAGES_AND_ITEMS = BASE_URL + "packages"
export let SAVE_QUOTATION=BASE_URL+"securequotation"
export let EDIT_QUOTATION=BASE_URL+"edit_quotation"
export let SAVE_RECEIPTS=BASE_URL+"save_receipt"
export let GET_QUOTATION=BASE_URL+"quotations"
export let GET_RECEIPTS=BASE_URL+"receipts"

/*
  Generated class for the ConstantProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConstantProvider {

  constructor() {
    console.log('Hello ConstantProvider Provider');
  }

}
