import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import * as URLS from '../../providers/constant/constant';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject, SQLiteDatabaseConfig } from '@ionic-native/sqlite';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {
  public myHttp: Http;
  token: string;
  private isOpen = false;
  public storage: SQLiteObject;
  tableList = ['users'];
  userid: string;
  //, 'getCustomers', 'api_sync', 'postCustomers', 'catalogCategories', 'catagoryItems', 'packages',
  //'packageItems', 'quotation', 'quotationItems'

  private dbstatus = 0;
  apiSyncList = [{ apiName: 'GET_CUSTOMERS', lastUpdatedTime: '0000-00-00 00:00:00' }, { apiName: 'GET_CATEGORIES', lastUpdatedTime: '0000-00-00 00:00:00' }, { apiName: 'GET_CATEGORIES_ITEMS', lastUpdatedTime: '0000-00-00 00:00:00' }, { apiName: 'GET_PACKAGES_AND_ITEMS', lastUpdatedTime: '0000-00-00 00:00:00' }, { apiName: 'GET_QUOTATION', lastUpdatedTime: '0000-00-00 00:00:00' }, { apiName: 'GET_RECEIPTS', lastUpdatedTime: '0000-00-00 00:00:00' }]

  constructor(public http: Http, public platform: Platform, sqlite: SQLite, ) {
    this.platform.ready().then(() => {
      console.log('Hello Database Provider');
      this.myHttp = http;
    
      let configdb: SQLiteDatabaseConfig = {
        "name": "data.db",
        "location": "default"
      };
      if (!this.isOpen) {
        sqlite.create(configdb).then((data: SQLiteObject) => {
          this.isOpen = true;
          this.storage = data;
          this.dbstatus++;
          this.createTables();
        }, (error) => {
          console.error("Unable to open database", error);
        });
      }

    })

  }


  mainCallBack: Function;
  /**
   * setMainCallBack
   */
  public setMainCallBack(callBack: Function) {
    this.mainCallBack = callBack;
  }


  readyCallBack: Function;
  /**
   * isReady
   */
  public isReady(callBack: Function) {

    if (this.dbstatus >= 2) {
      callBack.call(this);
    } else {
      this.readyCallBack = callBack;
    }


  }


  isDynamicSyncing = false;
  /**
     * serverSyncDynamicData
     */

  public serverSyncDynamicData() {
    return new Promise((resolve, reject) => {
      console.log("this data 0")
      if (this.isDynamicSyncing) {
        resolve()
        return;
      }
      this.isDynamicSyncing = true;
      console.log("this data 1")
      // api syncing for teacher login

      this.syncQuotationApi('quotation').then(data => {
        console.log("sync quotation api ")


        this.syncReceiptsApi().then(data => {
          console.log("sync receipt api ")
          this.syncQuotationApi('tempEditquotation').then(data => {
            console.log("sync tempEditquotation api ")
            this.isDynamicSyncing = false;
            
            resolve();
          }, err => {
            resolve();
            console.log(err);
            this.isDynamicSyncing = false;
          })
        }, err => {
          resolve();
          console.log(err);
          this.isDynamicSyncing = false;
        })

      }, err => {
        resolve();
        console.log(err);
        this.isDynamicSyncing = false;
      })


    })
  }


  public createTables() {
    this.storage.executeSql("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, token INTEGER, main_id TEXT, role TEXT)", {}).then((data) => {
      console.log("TABLE CREATED: users", data);
      this.dbstatus++;
      this.storage.executeSql("CREATE TABLE IF NOT EXISTS getCustomers (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,mobileNo TEXT,alternateMobileNo TEXT,createdDate TEXT,updatedDate TEXT,address Text,email TEXT,quotationId TEXT,status TEXT)", {}).then((data) => {
        this.dbstatus++;
        console.log("TABLE CREATED: getCustomers table", data);
        this.storage.executeSql("CREATE TABLE IF NOT EXISTS api_sync (id INTEGER PRIMARY KEY AUTOINCREMENT,apiName TEXT,lastUpdatedTime TEXT)", {}).then((data) => {
          this.dbstatus++;
          console.log("TABLE CREATED: api_sync table", data);

          this.storage.executeSql("CREATE TABLE IF NOT EXISTS catalogCategories (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,updatedDate TEXT,status TEXT)", {}).then((data) => {
            this.dbstatus++;
            console.log("TABLE CREATED: catalogCategories table", data);
            this.storage.executeSql("CREATE TABLE IF NOT EXISTS catagoryItems (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,categoryId TEXT,updatedDate TEXT,status TEXT)", {}).then((data) => {
              this.dbstatus++;
              console.log("TABLE CREATED: catagoryItems table", data);
              this.storage.executeSql("CREATE TABLE IF NOT EXISTS packages (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT)", {}).then((data) => {
                this.dbstatus++;
                console.log("TABLE CREATED: packages table", data);
                this.storage.executeSql("CREATE TABLE IF NOT EXISTS packageItems (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,packageName TEXT,categoryId TEXT,categoryName TEXT,mainPackageName TEXT)", {}).then((data) => {
                  this.dbstatus++;
                  console.log("TABLE CREATED: packages table", data);
                  //id-quotationId,Main_id:PackageId
                  // this.storage.executeSql("CREATE TABLE IF NOT EXISTS viewQuotations (localid INTEGER PRIMARY KEY AUTOINCREMENT,id Text,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT, status TEXT)", {}).then((data) => {
                  //   this.dbstatus++;
                  //   console.log("TABLE CREATED: viewQuotations table", data);
                  //   this.storage.executeSql("CREATE TABLE IF NOT EXISTS viewQuotationItems (id INTEGER PRIMARY KEY AUTOINCREMENT,quotationId TEXT,itemId TEXT,itemName TEXT )", {}).then((data) => {
                  //     this.dbstatus++;
                  //     console.log("TABLE CREATED: viewQuotationItems table", data);
                  // kept local id name for reusing a syncing function for api quotation for editing and first time sync
                  this.storage.executeSql("CREATE TABLE IF NOT EXISTS quotation (id INTEGER PRIMARY KEY AUTOINCREMENT,quotation_main_id INTEGER,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT,createdDate TEXT, status TEXT)", {}).then((data) => {
                    this.dbstatus++;
                    console.log("TABLE CREATED: quotation table", data);
                    this.storage.executeSql("CREATE TABLE IF NOT EXISTS tempEditquotation (localid INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT, status TEXT)", {}).then((data) => {
                      this.dbstatus++;
                      console.log("TABLE CREATED: tempEditquotation", data);
                      this.storage.executeSql("CREATE TABLE IF NOT EXISTS quotationItems (id INTEGER PRIMARY KEY AUTOINCREMENT,quotation_main_id TEXT,quotationId TEXT,itemId TEXT,itemName TEXT )", {}).then((data) => {
                        this.dbstatus++;
                        console.log("TABLE CREATED: quotationItems table", data);
                        this.storage.executeSql("CREATE TABLE IF NOT EXISTS receipts (receiptNo INTEGER PRIMARY KEY AUTOINCREMENT,quotationId TEXT,localquotationId TEXT,receiptDate TEXT ,customerName TEXT,amount TEXT,paymentMode TEXT,chequeDate TEXT,amountWords TEXT,paymentType Text,addedBy TEXT,chequeNo TEXT ,serverReceipt TEXT, isSync TEXT)", {}).then((data) => {
                          this.dbstatus++;
                          console.log("TABLE CREATED: receipts table", data);
                          if (this.readyCallBack != null) {
                            this.readyCallBack.call(this);
                          }
                        }, (error) => {
                          console.error("Unable to execute sql", error);
                        })
                      }, (error) => {
                        console.error("Unable to execute sql", error);
                      })
                    }, (error) => {
                      console.error("Unable to execute sql", error);
                    })
                  }, (error) => {
                    console.error("Unable to execute sql", error);
                  })
                }, (error) => {
                  console.error("Unable to execute sql", error);
                })
              }, (error) => {
                console.error("Unable to execute sql", error);
              })
            }, (error) => {
              console.error("Unable to execute sql", error);
            })

            //   }, (error) => {
            //     console.error("Unable to execute sql", error);
            //   })
            // }, (error) => {
            //   console.error("Unable to execute sql", error);
            // })
          }, (error) => {
            console.error("Unable to execute sql", error);
          })
        }, (error) => {
          console.error("Unable to execute sql", error);
        })
      }, (error) => {
        console.error("Unable to execute sql", error);
      })
    }, (error) => {
      console.error("Unable to execute sql", error);
    })
  }


  /**
     * serverSyncDynamicData
     */
  public serverSyncStaticData() {
    return new Promise((resolve, reject) => {
      this.getCustomerApi().then(() => {
        console.log("Customer synced");
        this.viewQuotationsApi().then((data) => {
          console.log("get quotation api ")
          this.getReceiptViewApi().then((data) => {
            console.log("recepits  api");

            this.getPackagesApi().then(() => {
              console.log("Packages synced");
              this.getCategoryApi().then(() => {
                console.log("Category synced");
                this.getCategoryItemsApi().then(() => {
                  console.log("Category Items synced");

                  resolve();
                }, err => {
                  resolve();
                })
              }, err => {
                resolve();
              })
            }, err => {
              resolve();
            })
          }, err => {
            resolve();
          })
        }, err => {
          resolve();
        })
      }, err => {
        resolve();

      })
    })
  }


  /**
       * signIn
       */
  public signIn(uname: string, paswd: string) {
    console.log("signing in");
    return new Promise<boolean>((resolve, reject) => {
      var body = JSON.stringify({ 'secure_uname': uname, 'secure_pass': paswd });
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      this.myHttp.post(URLS.SIGN_IN, body, options).pipe(map(res => res.json())).subscribe(data => {
        let temp = "";
        temp = data;
        console.log("signin in result ");
        console.log(temp);
        var token = "dummytoken";
        if (data.token != null) {
          token = data.token;
          var main_id = data.main_user;
          var role = "";
          role = data.role;
          if (token != null && token.length > 0 && main_id != null && main_id.length > 0) {
            console.log("signin in successfull token : " + token);
            this.token = token;
            this.userid = main_id;
            this.storage.executeSql("INSERT INTO users VALUES(null,?,?,?)", [token, main_id, role]).then((data) => {
              this.token = token;

              this.apisyncTable().then((data) => {
                if (data > 0) {
                  console.log("enter >0 data " + data)
                  resolve(true);
                  this.mainCallBack(role);
                }
                else {
                  console.log("enter 0 data " + data)

                  let index = this.apiSyncList.length;
                  for (let i = 0; i < this.apiSyncList.length; i++) {
                    this.storage.executeSql("INSERT INTO api_sync VALUES(null,?,?)", [this.apiSyncList[i].apiName, this.apiSyncList[i].lastUpdatedTime]).then((data) => {
                      console.log(data)
                      index--;
                      if (index == 0) {
                        resolve(true);
                        this.mainCallBack(role);
                      }

                    },
                      err => {
                        console.log(err)
                      })

                  }
                }

              })

            }, (error) => {
              console.error(error);
              reject(error);
            });
          } else {
            console.log('invalid token/main_user reveived from server on signin ' + data);
            resolve(false);
          }
        }
        else {
          console.log('invalid token/main_user reveived from server on signin ' + data);
          resolve(false);
        }

      }, err => {
        console.log("error siging in");
        console.log(err)
        reject(err);
      });

    })
  }




  public apisyncTable() {
    return new Promise<number>((resolve, reject) => {
      this.storage.executeSql("SELECT * FROM api_sync", []).then((data) => {
        if (data.rows.length > 0) {
          resolve(data.rows.length)
        }
        else {
          resolve(0)

        }
      })


    })
  }
  /**
     * isSignedIn
     */

  public isSignedIn() {
    console.log("is signed in");
    return new Promise<string>((resolve, reject) => {


      this.storage.executeSql("SELECT * FROM users LIMIT 1", []).then((data) => {
        console.log("is signed in1");
        console.log("data issigned in +" + JSON.stringify(data));
        console.log("rows" + JSON.stringify(data.rows.item(0)))
        if (data.rows.length > 0) {
          console.log("is signed in2");
          this.userid = data.rows.item(0).main_id;


          //  if (people[0].token != null && people[0].token.length > 0) {

          resolve("logged");
          // } else {
          // console.log("is signed in3");
          // resolve("wrong");
          // }
        } else {
          console.log("is not signed in4");
          resolve(null);
        }
      }, (error) => {
        console.log("is signed in5");

        console.error(error);
        reject(error);
      });
    });
  }



  /**
     * Logout
     */
  public Logout() {
    return new Promise<string>((resolve, reject) => {

      for (let i = 0; i < this.tableList.length; i++) {
        this.storage.executeSql("delete  from " + this.tableList[i], []).then((data) => {
          console.log(data);
          console.log("deleted data table " + this.tableList[i])
          resolve()
        },
          err => {
            console.log(err)
            resolve()
          })
      }
    })
  }


  getItemsBySearch(val) {
    return new Promise<any>((resolve, reject) => {
      //,main_id TEXT,name TEXT,categoryId TEXT,updatedDate TEXT,status TEXT
      this.storage.executeSql("SELECT * FROM catagoryItems WHERE name  LIKE ?", [val.toLowerCase() + '%']).then((data) => {
        let items = []

        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            items.push({
              name: data.rows.item(i).name,
              main_id: data.rows.item(i).main_id,
              packageName: ""
            }
            )
          }
          resolve(items)

        }
        else {
          resolve(items)
        }

      },
        err => {
          console.log(err)
        })


    })
  }


  /**
   * getApis
   */
  public getApis(apiUrl, apiName) {
    return new Promise<any>((resolve, reject) => {
      console.log("api URL" + apiUrl)
      this.storage.executeSql("select DATETIME(lastUpdatedTime,'-2 minutes') as Dt,lastUpdatedTime from api_sync WHERE apiName=?", [apiName]).then((data) => {
        console.log("First time date fetched get  " + apiName);
        let updateTime: string = null;
        console.log(data.rows.length)
        console.log(JSON.stringify(data.rows.item(0)))
        if (data.rows.length > 0) {
          if (data.rows.item(0).Dt != null) {
            updateTime = data.rows.item(0).Dt;
          }
          else {
            updateTime = '0000-00-00 00:00:00';
          }

          console.log(" updates time" + updateTime)
          let URL: string;
          if (apiName != 'GET_CUSTOMERS') {

            URL = apiUrl + '?updated_date=' + updateTime;
            console.log("enter 2")
          }
          else {
            URL = apiUrl + '?update=' + updateTime;
            console.log("enter 1")

          }
          this.commonApiFunc(URL).then((data) => {
            resolve(data)

          }, err => {
            console.log(err);

          })
        }
        else {
          console.log("ORDER STATUS 1")
          let URL: string;
          if (apiName != 'GET_CUSTOMERS') {
            URL = apiUrl + '?updated_date=' + '0000-00-00 00:00:00'

          }
          else {
            URL = apiUrl + '?update=' + '0000-00-00 00:00:00'

          }
          this.commonApiFunc(URL).then((data) => {
            resolve(data)
            this.storage.executeSql("INSERT INTO api_sync VALUES(null,?,DATETIME('now','localtime'))", [apiName]).then(() => {
              console.log("inserted data into api sync")

            },
              err => {
                console.log(err)
              });
          }, err => {
            console.log(err);

          })



        }

      },
        err => {
          console.log(err)
        })

    })
  }

  commonApiFunc(URL) {
    return new Promise<any>((resolve, reject) => {
      let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token });
      let options = new RequestOptions({ headers: headers });
      this.myHttp.get(URL, options).pipe(map(res => res.json())).subscribe(data => {
        resolve(data)

      },
        err => {
          console.log(err)
          resolve()
        })

    })
  }



  /**
       * deleteAllFromTable
       */
  public deleteAllFromTable(tableName: string) {
    return new Promise((resolve, reject) => {
      this.storage.executeSql("DELETE FROM " + tableName, []).then((data) => {
        resolve(data.rows.length);
      }, (error) => {
        console.error(error);
        reject(error);
      });
    });
  }


  public getReceiptViewApi() {
    return new Promise<boolean>((resolve, reject) => {
      this.getApis(URLS.GET_RECEIPTS, 'GET_RECEIPTS').then((data) => {
        let len = data.length;
        let index = len;
        console.log(" get  receipts api ")
        console.log(data)
        if (len > 0) {
          this.storage.executeSql("UPDATE api_sync SET lastUpdatedTime=DATETIME('now','localtime') WHERE apiName=?", ['GET_RECEIPTS']).then((data1) => {
            console.log(data1)

          })
          let i = len;

          let fnct = () => {
            //       this.storage.executeSql("CREATE TABLE IF NOT EXISTS receipts (receiptNo INTEGER PRIMARY KEY AUTOINCREMENT,quotationId TEXT,localquotationId TEXT,receiptDate TEXT ,customerName TEXT,amount TEXT,paymentMode TEXT,chequeDate TEXT,amountWords TEXT,paymentType Text,addedBy TEXT,chequeNo TEXT ,serverReceipt TEXT, isSync TEXT)",
            i--;
            if (i >= 0) {
              this.deleteAllFromTableConditional('receipts', 'quotationId', data[i].quotation_id).then(() => {
                fnct.call(this);
              });
            } else {
              console.log("e neter in api rc ")
              console.log(JSON.stringify(data[i]))
              for (let i = 0; i < len; i++) {

                this.storage.executeSql("INSERT INTO receipts VALUES(null,?,?,?,?,?,?,?,?,?,?,?,?,?)", [data[i]["quotation_id"], 0, data[i]["receipt_date"], data[i]["customer_name"], data[i]["amount"], data[i]["payment_mode"], data[i]["cheque_date"], data[i]["amount_words"], data[i]["payment_type"], 0, data[i]["cheque_no"], data[i]["id"], true]).then((dbData) => {
                  console.log(dbData)
                  console.log("enter 22")
                  index--;
                  if (index == 0) {
                    resolve()
                  }

                },
                  err => {
                    index--;
                    if (index == 0) {
                      resolve()
                    }

                    console.log(err)
                  })

              }
            }

          }
          {
            fnct.call(this)

          }


        }
        else {
          resolve(false)

        }

      }).catch(
        (err) => {
          console.log("enter in error getCategoryApi")
          console.log(err);
          resolve(false)
        })



    })
  }






  /**
   * viewQuotationsApi
   */
  public viewQuotationsApi() {
    return new Promise<boolean>((resolve, reject) => {
      this.getApis(URLS.GET_QUOTATION, 'GET_QUOTATION').then((data) => {
        console.log(" get  QUOTATION api ")
        console.log(data)
        let dataKeys = Object.keys(data)
        let len = dataKeys.length
        let index = len;

        // console.log("TABLE CREATED: viewQuotations table", data);
        // this.storage.executeSql("CREATE TABLE IF NOT EXISTS viewQuotationItems (id INTEGER PRIMARY KEY AUTOINCREMENT,quotationId TEXT,itemId TEXT,itemName TEXT )", {}).then((data)
        console.log("data length" + index)
        if (len > 0) {
          this.storage.executeSql("UPDATE api_sync SET lastUpdatedTime=DATETIME('now','localtime') WHERE apiName=?", ['GET_QUOTATION']).then((data1) => {
            console.log(data1)

          })
          let i = dataKeys.length;

          let fnct = () => {
            //          this.storage.executeSql("CREATE TABLE IF NOT EXISTS quotation (id INTEGER PRIMARY KEY AUTOINCREMENT,quotation_main_id INTEGER,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT, status TEXT)", {}).then((data) => {

            // console.log("TABLE CREATED: tempEditquotation", data);
            // this.storage.executeSql("CREATE TABLE IF NOT EXISTS quotationItems (id INTEGER PRIMARY KEY AUTOINCREMENT,quotation_main_id TEXT,quotationId TEXT,itemId TEXT,itemName TEXT )", {}).then
            i--;
            if (i >= 0) {
              this.deleteAllFromTableConditional('quotation', 'quotation_main_id', dataKeys[i]).then(() => {
                this.deleteAllFromTableConditional('quotationItems', 'quotation_main_id', dataKeys[i]).then(() => {
                  fnct.call(this);
                });
              });
            } else {
              console.log("e neter in api ")
              console.log(JSON.stringify(data[dataKeys[0]]["quo_detail"]))
              for (let i = 0; i < len; i++) {
                let dt = data[dataKeys[i]]["quo_detail"]['occasion_datetime'].split(' ')
                this.storage.executeSql("INSERT INTO quotation VALUES(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [data[dataKeys[i]]["quo_detail"]['id'], data[dataKeys[i]]["quo_detail"]['package_id'], data[dataKeys[i]]["quo_detail"]['app_cust_id'], data[dataKeys[i]]["quo_detail"]['customer_name'], data[dataKeys[i]]["quo_detail"]['address'], data[dataKeys[i]]["quo_detail"]['mobile1'], data[dataKeys[i]]["quo_detail"]['mobile2'], data[dataKeys[i]]["quo_detail"]['email'], data[dataKeys[i]]["quo_detail"]['occasion_venue'], data[dataKeys[i]]["quo_detail"]['occasion'], dt[0] + 'T' + dt[1] + '.830Z', data[dataKeys[i]]["quo_detail"]['plate_no'], data[dataKeys[i]]["quo_detail"]['plate_amount'], data[dataKeys[i]]["quo_detail"]['additional_cost'], data[dataKeys[i]]["quo_detail"]['total'], data[dataKeys[i]]["quo_detail"]['message'], "", data[dataKeys[i]]["quo_detail"]['created_date'], "completed"]).then((dbData) => {
                  console.log(dbData)
                  console.log("enter 22")
                  console.log(data[dataKeys[i]].items)
                  let tempItems=[];
                  data[dataKeys[i]].items.forEach((element) => {
                    tempItems.unshift(element)
                  })
                  console.log("temp items");
                  console.log(tempItems)
                  tempItems.forEach((element) => {
                    console.log("enter 33")

                    this.storage.executeSql("INSERT INTO quotationItems VALUES(null,?,?,?,?)", [element.quotation_id, dbData.insertId, element.item_id, element.item_name]).then((data) => {
                      console.log("44")
                      index--;
                      if (index == 0) {
                        resolve(true)
                      }
                    },
                      err => {
                        index--;
                        if (index == 0) {
                          resolve(true)
                        }
                        console.log(err)
                      })
                  })


                },
                  err => {
                    index--;
                    if (index == 0) {
                      resolve(true)
                    }
                    console.log(err)
                  })

              }
            }

          }
          {
            fnct.call(this)

          }


        }
        else {
          resolve(false)

        }

      }).catch(
        (err) => {
          console.log("enter in error getCategoryApi")
          console.log(err);
          resolve(false)
        })



    })
  }

  public getCategoryItemsApi() {
    return new Promise<boolean>((resolve, reject) => {
      this.getApis(URLS.GET_CATEGORIES_ITEMS, 'GET_CATEGORIES_ITEMS').then((data) => {
        console.log("  category items ")
        console.log(data)
        let categoryKey = Object.keys(data);
        let len = categoryKey.length
        //  this.storage.executeSql("CREATE TABLE IF NOT EXISTS catalogCategories (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,updatedDate TEXT,status TEXT)", {}).then((data) => {
        // this.dbstatus++;
        // console.log("TABLE CREATED: catalogCustomers table", data);
        // this.storage.executeSql("CREATE TABLE IF NOT EXISTS catagoryItems (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,categoryId TEXT,updatedDate TEXT,status TEXT)", {}).then((data) => {
        //   this.dbstatus++;



        console.log("data length" + len)
        if (len > 0) {
          console.log("enter111")
          this.storage.executeSql("UPDATE api_sync SET lastUpdatedTime=DATETIME('now','localtime') WHERE apiName=?", ['GET_CATEGORIES_ITEMS']).then((data1) => {
            console.log(data1)

          })
          let catlength = len;
          let i = catlength;
          console.log("i  " + i);

          let fnct = () => {

            i--;

            if (i >= 0) {

              this.deleteAllFromTableConditional('catagoryItems', 'categoryId', data[categoryKey[i]][0].category_id).then(() => {
                console.log("enter 3")

                fnct.call(this);

              },
                err => {
                  fnct.call(this);
                  console.log(err)
                });



            } else {

              for (let i = 0; i < len; i++) {

                let itemsLength = data[categoryKey[i]].length;
                console.log("Items Length")
                catlength--;
                for (let j = 0; j < data[categoryKey[i]].length; j++) {
                  let Status: number = +data[categoryKey[i]][j].id;
                  if (Status > 0) {
                    this.storage.executeSql("INSERT INTO catagoryItems VALUES(null,?,?,?,?,?)", [data[categoryKey[i]][j].id, data[categoryKey[i]][j].item, data[categoryKey[i]][j].category_id, data[categoryKey[i]][j].updated_date, data[categoryKey[i]][j].status]).then((data) => {
                      console.log(data);
                      console.log("inserted in items cat")
                      itemsLength--;

                      if (itemsLength == 0 && catlength == 0) {
                        resolve(true)


                      }

                    },
                      err => {
                        itemsLength--;

                        if (itemsLength == 0) {
                          resolve(true)
                        }
                        console.log(err)
                      })
                  } else {
                    itemsLength--;

                    if (itemsLength == 0) {
                      resolve(true)
                    }
                  }

                }

              }

            }
          }
          {
            fnct.call(this)

          }
        }
        else {
          resolve(false)

        }

      }).catch(
        (err) => {
          console.log("enter in error getCategoryItemsApi")
          console.log(err);
          resolve(false)
        })


    })
  }



  /**
   * getCategoryApi
   */
  public getCategoryApi() {
    return new Promise<boolean>((resolve, reject) => {
      this.getApis(URLS.GET_CATEGORIES, 'GET_CATEGORIES').then((data) => {
        console.log("  category  ")
        console.log(data)
        let len = data.length
        let index = len;
        //  this.storage.executeSql("CREATE TABLE IF NOT EXISTS catalogCategories (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,updatedDate TEXT,status TEXT)", {}).then((data) => {
        // this.dbstatus++;
        // console.log("TABLE CREATED: catalogCustomers table", data);
        // this.storage.executeSql("CREATE TABLE IF NOT EXISTS catagoryItems (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,categoryId TEXT,updatedDate TEXT,status TEXT)", {}).then((data) => {
        //   this.dbstatus++;
        console.log("data length" + index)
        if (len > 0) {
          this.storage.executeSql("UPDATE api_sync SET lastUpdatedTime=DATETIME('now','localtime') WHERE apiName=?", ['GET_CATEGORIES']).then((data1) => {
            console.log(data1)

          })
          let i = data.length;

          let fnct = () => {

            i--;
            if (i >= 0) {
              this.deleteAllFromTableConditional('catalogCategories', 'main_id', data[i].id).then(() => {
                fnct.call(this);
              });
            } else {
              for (let i = 0; i < len; i++) {
                let Status: number = +data[i].status;

                if (Status > 0) {
                  this.storage.executeSql("INSERT INTO catalogCategories VALUES(null,?,?,?,?)", [data[i].id, data[i].name, data[i].updated_date, data[i].status]).then((data) => {
                    console.log(data)
                    index--;

                    if (index == 0) {
                      resolve(true)


                    }

                  },
                    err => {
                      index--;
                      if (index == 0) {
                        resolve(true)
                      }
                      console.log(err)
                    })

                }
                else {
                  index--;
                  if (index == 0) {
                    resolve(true)
                  }
                }
              }
            }

          }
          {
            fnct.call(this)

          }


        }
        else {
          resolve(false)

        }

      }).catch(
        (err) => {
          console.log("enter in error getCategoryApi")
          console.log(err);
          resolve(false)
        })



    })
  }


  /**
   * getCustomerApi
   */
  public getCustomerApi() {
    return new Promise<boolean>((resolve, reject) => {
      this.getApis(URLS.GET_CUSTOMERS, 'GET_CUSTOMERS').then((data) => {
        console.log(" data customer ")
        console.log(data)
        //getCustomers (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,mobileNo TEXT,alternateMobileNo TEXT,createdDate TEXT,updatedDate TEXT,address Text,email TEXT,status TEXT)", {}).then((data) => {
        let len = data.length
        let index = len;
        console.log("data length" + index)
        if (len > 0) {
          this.storage.executeSql("UPDATE api_sync SET lastUpdatedTime=DATETIME('now','localtime') WHERE apiName=?", ['GET_CUSTOMERS']).then((data1) => {
            console.log(data1)

          })
          let i = data.length;
          let fnct = () => {
            i--;
            if (i >= 0) {
              this.deleteAllFromTableConditional('getCustomers', 'main_id', data[i].id).then(() => {
                fnct.call(this);

              })
            } else {

              for (let i = 0; i < len; i++) {
                //INSERT INTO sch_api_updation VALUES(null,?,?)"
                let Status: number = +data[i].status;

                if (Status > 0) {

                  this.storage.executeSql("INSERT INTO getCustomers VALUES(null,?,?,?,?,?,?,?,?,?,?)", [data[i].id, data[i].customer_name, data[i].mobile, data[i].alt_mobile, data[i].created_date, data[i].updated_date, data[i].address, data[i].email_id, "", data[i].status]).then((data) => {
                   console.log("customer data insrted")
                    console.log(data)
                    index--;

                    if (index == 0) {
                      resolve(true)


                    }

                  },
                    err => {
                      index--;
                      if (index == 0) {
                        resolve(true)
                      }
                      console.log(err)
                    })

                }
                else {
                  index--;
                  if (index == 0) {
                    resolve(true)
                  }
                }

              }

            }
          }
          {
            fnct.call(this)

          }

        }
        else {
          resolve(false)

        }

      }).catch(
        (err) => {
          console.log("enter in error")
          console.log(err);
          resolve(false)
        })


    })
  }

  saveCustomerLocalDb(quotation, quotationId) {
    return new Promise<boolean>((resolve, reject) => {
      //quotation.customerId, quotation.name, quotation.address, quotation.mobile, quotation.alternateMobileNo, quotation.email, 
      //(id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,mobileNo TEXT,alternateMobileNo TEXT,createdDate TEXT,updatedDate TEXT,address Text,email TEXT,quotationId TEXT,status TEXT)"
      console.log(" customer data")
      console.log(quotation.customerId.length)
      if (quotation.customerId.length>0) {
        console.log("enter in excisting")
        this.storage.executeSql("Update  getCustomers  SET name=?,mobileNo=?,alternateMobileNo=?,address=?,email=? , createdDate=?  where main_id=?", [quotation.name, quotation.mobile, quotation.alternateMobileNo, quotation.address, quotation.email, new Date().toLocaleDateString("en-US"),quotation.customerId]).then((data) => {
          console.log("enter in update");
          console.log(data)
          resolve(true)
        },
          err => {
            resolve(true)
            console.log(err)
          })
      }
      else {
        console.log("enter in new")
        
        this.storage.executeSql("INSERT INTO getCustomers VALUES(null,?,?,?,?,?,?,?,?,?,?)", ["", quotation.name, quotation.mobile, quotation.alternateMobileNo, new Date().toLocaleDateString("en-US"), "", quotation.address, quotation.email, quotationId, 1]).then((data) => {
          console.log(data)
          resolve(true)
        },
          err => {
            resolve(true)
            console.log(err)
          })
      }



    })
  }
  /**
     * deleteAllFromTableConditional
     */
  public deleteAllFromTableConditional(tableName: string, fieldName, itemId) {
    return new Promise((resolve, reject) => {

      //console.log("delete from " + tableName + " where main id " + itemId);
      this.storage.executeSql("DELETE FROM " + tableName + " WHERE " + fieldName + "=?", [itemId]).then((data) => {
        resolve(data.rows.length);
      }, (error) => {
        console.error(error);
        reject(error);
      });


    });
  }

  /**
   * getPackagesApi
   */
  public getPackagesApi() {
    return new Promise<boolean>((resolve, reject) => {

      this.getApis(URLS.GET_PACKAGES_AND_ITEMS, 'GET_PACKAGES_AND_ITEMS').then((dataApi) => {
        console.log("data fetched packages")
        console.log(dataApi)
        let packagesName = Object.keys(dataApi);
        console.log(packagesName);
        let packageLength = packagesName.length;
        //packages (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT)", {}).then((data) => {
        // this.dbstatus++;
        // console.log("TABLE CREATED: packages table", data);
        // this.storage.executeSql("CREATE TABLE IF NOT EXISTS packageItems (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,packageName TEXT,categoryId TEXT,categoryName TEXT,mainPackageName)", {}
        if (packagesName.length > 0) {
          console.log(" package name length more than 0")
          this.storage.executeSql("UPDATE api_sync SET lastUpdatedTime=DATETIME('now','localtime') WHERE apiName=?", ['GET_PACKAGES_AND_ITEMS']).then((data1) => {
            console.log(" sync packegs time updated")
            console.log(data1)
          })
          let i = packagesName.length;
          let fnct = () => {
            i--;
            if (i >= 0) {
              this.deleteAllFromTableConditional('packages', 'main_id', dataApi[packagesName[i]][0]['package_id']).then((data) => {
                this.deleteAllFromTableConditional('packageItems', 'mainPackageName', packagesName[i]).then((data) => {
                  fnct.call(this)

                  console.log(data)
                }, err => {
                  console.log(err)
                  fnct.call(this)
                })
              }, err => {
                console.log(err)
              })

            } else {
              for (let i = 0; i < packagesName.length; i++) {
                console.log(" enter in loop1")
                console.log(dataApi[packagesName[i]][0]['package_id'])
                this.storage.executeSql("INSERT INTO packages VALUES(null,?,?)", [dataApi[packagesName[i]][0]['package_id'], packagesName[i]]).then((data) => {
                  console.log(data);
                  packageLength--;
                  if (packageLength == 0) {
                    let packlength = packagesName.length;
                    for (let i = 0; i < packagesName.length; i++) {
                      let itemsLength = dataApi[packagesName[i]].length;
                      console.log(JSON.stringify(dataApi[packagesName[i]]))
                      packlength--;
                      for (let j = 0; j < dataApi[packagesName[i]].length; j++) {
                        this.storage.executeSql("INSERT INTO packageItems VALUES(null,?,?,?,?,?,?)", [dataApi[packagesName[i]][j]['item_id'], dataApi[packagesName[i]][j]['item_name'], dataApi[packagesName[i]][j]['p_name'], dataApi[packagesName[i]][j]['category_id'], dataApi[packagesName[i]][j]['cat_name'], packagesName[i]]).then((data) => {
                          itemsLength--;
                          console.log(" enter in loop3 " + itemsLength)
                          if (packlength == 0 && itemsLength == 0) {
                            console.log("final RESOLVE")
                            resolve(true)
                          }


                        }, err => {
                          console.log(err)
                        })
                      }
                    }
                  }
                }, err => {
                  console.log(err)
                })
              }
            }

          }
          {
            fnct.call(this)

          }
        }
        else {
          resolve(false)
        }

      }).catch(
        (err) => {
          console.log("enter in error packages")
          console.log(err);
          resolve(false)
        })


    })
  }

  public getpackagsDb() {
    return new Promise<{}[]>((resolve, reject) => {
      this.storage.executeSql("select * from packages", []).then((data) => {
        let packges = [{ "id": String, "main_id": String, "name": String }];

        let len = data.rows.length
        if (len > 0) {
          for (let i = 0; i < len; i++) {
            packges[i] = (data.rows.item(i))

          }
          resolve(packges)
        }
        else {
          resolve(packges)
        }

      }, err => {
        console.log(err)
      })

    })
  }




  /**
 * quotationCount
 */
  public quotationCount() {
    return new Promise<number>((resolve, reject) => {
      this.storage.executeSql("SELECT COUNT(*) rc FROM quotation ", []).then((data) => {
        console.log("quotaioncount in database enter");
        if (data.rows.length > 0) {
          resolve(data.rows.item(0).rc)
        } else {
          resolve(0);
        }

      }, (error) => {
        console.error(error);
        reject(error);
      });
    });
  }


  /**
 * customerCount
 */
  public customerCount() {
    return new Promise<number>((resolve, reject) => {
      this.storage.executeSql("SELECT COUNT(*) rc FROM getCustomers WHERE status='1'", []).then((data) => {
        console.log("customercount in database enter");
        if (data.rows.length > 0) {
          resolve(data.rows.item(0).rc)
        } else {
          resolve(0);
        }

      }, (error) => {
        console.error(error);
        reject(error);
      });
    });
  }

  /**
   * getcatergoriesItemsDb
   */
  public getcatergoriesItemsDb() {
    return new Promise<{}[]>((resolve, reject) => {

      //  this.storage.executeSql("CREATE TABLE IF NOT EXISTS catalogCategories (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,updatedDate TEXT,status TEXT)", {}).then((data) => {

      // this.storage.executeSql("CREATE TABLE IF NOT EXISTS catagoryItems (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,categoryId TEXT,updatedDate TEXT,status TEXT)", {}).then((data) => {
      //   this.dbstatus++;
      this.storage.executeSql("select catalogCategories.name as categoryName,catagoryItems.name as categoryItemsName,catagoryItems.categoryId from catalogCategories LEFT JOIN catagoryItems ON catalogCategories.main_id = catagoryItems.categoryId where catagoryItems.status=?", [1]).then((data) => {
        console.log(" data category")
        for (let i = 0; i < data.rows.length; i++) {
          console.log(data.rows.item(i));

        }
        let categoryData = []
        if (data.rows.length > 0) {
          let datalength = data.rows.length;
          let lastCategoryId = 0;
          let CategoryId = 0;
          let items = {};
          let index = -1;

          console.log("enter 0")
          for (let i = 0; i < datalength; i++) {
            CategoryId = data.rows.item(i).categoryId
            if (CategoryId != lastCategoryId) {
              console.log("enter in if ")
              index++;
              let categoryName = { "name": data.rows.item(i).categoryName, icon: 'ios-add-circle-outline', showDetails: false, "Menu": [] }
              categoryData.push(categoryName);

            }
            items = { "name": data.rows.item(i).categoryItemsName }
            console.log("enter 3")

            categoryData[index]['Menu'].push(items);
            lastCategoryId = CategoryId
          }

          resolve(categoryData)
        }
        else {
          resolve(categoryData)
        }

      },
        err => {
          console.log(err)
        })



    })
  }

  /**
   * syncReceiptsApi(
   *  */
  public syncReceiptsApi() {
    return new Promise<boolean>((resolve, reject) => {
      // receiptNo INTEGER PRIMARY KEY AUTOINCREMENT,quotationId TEXT,receiptDate TEXT ,customerName TEXT,amount TEXT,paymentMode TEXT,chequeDate TEXT,amountWords TEXT,paymentType Text,addedBy TEXT,chequeNo TEXT ,serverReceipt TEXT, isSync TEXT)
      console.log("enter in req api ")

      this.storage.executeSql("select receipts.*,quotation.quotation_main_id from receipts left join quotation ON receipts.quotationId=quotation.quotation_main_id  where receipts.isSync=? AND quotation.status=?", [false, 'completed']).then((data) => {
        console.log("add reciept api 1");
        console.log(data.rows.length)

        if (data.rows.length > 0) {

          let receipts: Array<{ receipt_id: String, main_id: String, receipt_no: String, receipt_date: String, customer_name: String, amount: String, payment_mode: String, cheque_date: String, payment_type: String, amount_words: String, added_by: String, cheque_no: String }>;
          receipts = [];
          console.log("add reciept api")
          for (let i = 0; i < data.rows.length; i++) {
            console.log(data.rows.item(i)['quotation_main_id'])
            if (data.rows.item(i)['quotation_main_id'] > 0) {
              receipts.push({
                ['receipt_id']: data.rows.item(i)['receiptNo'],
                ['main_id']: data.rows.item(i)['quotation_main_id'],
                ['receipt_no']: data.rows.item(i)['receiptNo'],
                ['receipt_date']: data.rows.item(i)['receiptDate'],
                ['customer_name']: data.rows.item(i)['customerName'],
                ['amount']: data.rows.item(i)['amount'],
                ['payment_mode']: data.rows.item(i)['paymentMode'],
                ['cheque_date']: data.rows.item(i)['chequeDate'] ? data.rows.item(i)['chequeDate'] : "",
                ['payment_type']: data.rows.item(i)['paymentType'],
                ['amount_words']: data.rows.item(i)['amountWords'],
                ['added_by']: this.userid,
                ['cheque_no']: data.rows.item(i)['chequeNo'] ? data.rows.item(i)['chequeNo'] : "",
              })
            }


            // receipts[i]['quotation_id'] = data.rows.item(i)['quotationId'];
            // receipts[i]['main_id'] = data.rows.item(i)['receiptNo'];
            // receipts[i]['receipt_no'] = data.rows.item(i)['receiptNo'];
            // receipts[i]['receipt_date'] = data.rows.item(i)['receiptDate'];
            // receipts[i]['customer_name'] = data.rows.item(i)['customerName'];
            // receipts[i]['amount'] = data.rows.item(i)['quotationId'];
            // receipts[i]['payment_mode'] = data.rows.item(i)['quotationId'];
            // receipts[i]['cheque_date'] = data.rows.item(i)['chequeDate'];
            // receipts[i]['payment_type'] = data.rows.item(i)['quotationId'];
            // receipts[i]['amount_words'] = data.rows.item(i)['quotationId'];
            // receipts[i]['added_by'] = this.userid;
            // receipts[i]['cheque_no'] = data.rows.item(i)['quotationId'];

          }


          let body = JSON.stringify({ 'receipt': JSON.stringify(receipts) });
          console.log("bOdy");
          console.log(JSON.stringify(body))
          let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": "Bearer " + this.token });
          let options = new RequestOptions({ headers: headers });
          this.myHttp.post(URLS.SAVE_RECEIPTS, body, options).pipe(map(res => res.json())).subscribe(data => {
            console.log("reciept api data")
            console.log(data)
            let responseKey = Object.keys(data);
            let len = responseKey.length
            console.log("response key")
            console.log(responseKey[0])
            for (let i = 0; i < responseKey.length; i++) {
              this.storage.executeSql("Update  receipts  SET isSync=? ,serverReceipt=?  where receiptNo=?", [true, data[responseKey[i]], responseKey[i]]).then((data) => {
                console.log(data)
                len--;
                if (len == 0) {
                  resolve

                }
              },
                err => {
                  console.log(err);
                  resolve(err);
                })
            }

          },
            err => {
              console.log(err);
              resolve(err);
            })
        }
        else {
          resolve(false)
        }
      })


    })
  }


  /**
   * syncQuotation
   */
  public syncQuotationApi(tableName) {
    return new Promise<boolean>((resolve, reject) => {
      //  this.storage.executeSql("CREATE TABLE IF NOT EXISTS quotation (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT, status TEXT)", {}).then((data) => {


      //quotationItems (AUTOINCREMENT,quotation_main_id TEXT,quotationId TEXT,itemId TEXT,itemName TEXT

      //tempEditquotation (localid INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT, status TEXT)", 
      let sql: string;
      if (tableName == 'quotation') {
        sql = "select quotation.*,quotationItems.quotationId,quotationItems.itemId,quotationItems.id as localIdItem,quotationItems.itemName from quotation LEFT JOIN quotationItems ON quotation.id = quotationItems.quotationId where  quotation.id=(select id from quotation where status='pending' LIMIT 1)"
      }
      else {
        sql = "select tempEditquotation.*,quotationItems.quotationId,quotationItems.itemId,quotationItems.id as localIdItem,quotationItems.itemName from tempEditquotation LEFT JOIN quotationItems ON tempEditquotation.id = quotationItems.quotation_main_id where  tempEditquotation.id=(select id from tempEditquotation where status='pending' LIMIT 1)"
      }
      this.storage.executeSql(sql, []).then((data) => {

        let quotationData = []
        let datalength = data.rows.length;
        let lastQuotationId = "";
        let quotationId = "";
        let items = {};
        let index = -1;

        if (data.rows.length > 0) {
          console.log("data of quotation api")
          for (let i = 0; i < datalength; i++) {
            console.log(JSON.stringify(data.rows.item(i)))
            let date: string = (data.rows.item(i).occasionDateTime.split('T')[0]);
            let time = (data.rows.item(i).occasionDateTime.split('T')[1])
            date = date + " " + time.substr(0, 5)

            quotationId = data.rows.item(i).id
            if (quotationId != lastQuotationId) {
              console.log("enter in if ")
              index++;
              let quotationDataTemp = { quotation_id: data.rows.item(i).id, customer: { app_cust_id: data.rows.item(i).id, customer_id: data.rows.item(i).customerId, customer_name: data.rows.item(i).customerName, customer_address: data.rows.item(i).customerAddress, mobile1: data.rows.item(i).mobileNo, mobile2: data.rows.item(i).alternateMobileNo ? data.rows.item(i).alternateMobileNo : "", email: data.rows.item(i).emailId ? data.rows.item(i).emailId : "" }, package_id: data.rows.item(i).main_id, occasion: data.rows.item(i).occasion, occasion_datetime: date, occasion_venue: data.rows.item(i).venue, plate_no: data.rows.item(i).platesNumber, plate_amount: data.rows.item(i).plateAmount, additional_cost: data.rows.item(i).additionalCost, total: data.rows.item(i).total, quotation_status: data.rows.item(i).status, message: data.rows.item(i).message, quotation_items: [] }
              quotationData.push(quotationDataTemp);
            }
            items = { "id": data.rows.item(i).localIdItem, "item_id": data.rows.item(i).itemId, "item_name": data.rows.item(i).itemName }
            console.log("enter 3")

            quotationData[index]['quotation_items'].push(items);
            lastQuotationId = quotationId

          }
          console.log("quotation data")
          console.log(JSON.stringify(quotationData))
          // var body = JSON.stringify({ 'track': JSON.stringify(trackList) });

          let body = JSON.stringify({ 'quotation': JSON.stringify(quotationData) });
          let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": "Bearer " + this.token });
          let options = new RequestOptions({ headers: headers });
          if (tableName == 'quotation') {
            this.myHttp.post(URLS.SAVE_QUOTATION, body, options).pipe(map(res => res.json())).subscribe(data => {
              console.log("Api quotation response " + data)
              console.log(data);
              let responseKey = Object.keys(data)
              console.log(responseKey)

              console.log(JSON.stringify(data[responseKey[0]]))
              console.log(data[responseKey[0]]['customer_id'])
              //quotation_main_id TEXT,main_id TEXT,customerId
              this.storage.executeSql("Update  getCustomers  SET main_id=? where quotationId=?", [data[responseKey[0]]['customer_id'],responseKey[0]]).then(() => {
                console.log("enter in update customer db");
                console.log(data)
              },
                err => {

                  console.log(err)
                })
              this.storage.executeSql("Update  quotation  SET status=?,quotation_main_id=?,customerId=?  where id=?", ['completed', data[responseKey[0]]['quotation_id'], data[responseKey[0]]['customer_id'], responseKey[0]]).then((data22) => {
                console.log(data22)
          
                this.syncQuotationApi(tableName).then(() => {
                  resolve()
                  console.log("Api quptation pushed to server")
                },
                  err => {
                    console.log(err);
                    resolve(err);
                  })

              },
                err => {
                  console.log(err);
                  resolve(err);
                })

            },
              err => {
                console.log(err);
                resolve(err);
              })
          }
          else {
            console.log("enter in edit apiaaaaaaaaaaaa")
            this.myHttp.post(URLS.EDIT_QUOTATION, body, options).pipe(map(res => res.json())).subscribe(data => {
              console.log("Api edit quotation response " + data)
              console.log(data);
              // let responseKey = Object.keys(data)
              // console.log(responseKey)

              // console.log( JSON.stringify(data[responseKey[0]]))
              let responseKey = Object.keys(data)
              console.log("response key")
              console.log(data[responseKey[0]])
              this.deleteAllFromTableConditional('tempEditquotation', 'id', responseKey[0]).then(() => {

                console.log(data)
                this.syncQuotationApi(tableName).then(() => {
                  resolve()
                  console.log("Api edit quptation pushed to server")
                },
                  err => {
                    console.log(err);
                    resolve(err);
                  })

              })

            },
              err => {
                console.log(err);
                resolve(err);
              })
          }

        }
        else {
          resolve(false)
        }


      }, err => {
        console.log(err)
      })


    })
  }

  /**
   * getpackagesItemsDb
   */
  public getpackagesItemsDb() {
    return new Promise<{}[]>((resolve, reject) => {
      this.storage.executeSql("select packageItems.*,packages.main_id as packageId from packages LEFT JOIN packageItems ON packages.name = packageItems.mainPackageName", []).then((data) => {
        console.log(data.rows.item(0));
        data.rows.length;
        let packagesData = []
        if (data.rows.length > 0) {
          let datalength = data.rows.length;
          let lastMainPackageName = "";
          let mainPackageName = "";
          let items = {};
          let index = -1;

          console.log("enter 0")
          for (let i = 0; i < datalength; i++) {
            mainPackageName = data.rows.item(i).mainPackageName
            if (mainPackageName != lastMainPackageName) {
              console.log("enter in if ")
              index++;
              let packagename = { "name": data.rows.item(i).mainPackageName, packageId: data.rows.item(i).packageId, icon: 'ios-add-circle-outline', showDetails: false, "Menu": [] }
              packagesData.push(packagename);

            }
            items = { "main_id": data.rows.item(i).main_id, "name": data.rows.item(i).name, "packageName": data.rows.item(i).name, "categoryId": data.rows.item(i).categoryId, "categoryName": data.rows.item(i).categoryName, "mainPackageName": data.rows.item(i).mainPackageName }
            console.log("enter 3")

            packagesData[index]['Menu'].push(items);
            lastMainPackageName = mainPackageName
          }



          resolve(packagesData)
        }
        else {
          resolve(packagesData)
        }

      },
        err => {
          console.log(err)
        })



    })
  }



  getQuotationById(id) {
    return new Promise<{}>((resolve, reject) => {

      let sql: string;
      console.log("enter 2")

      // sql = 'select * from getCustomers';
      sql = "SELECT * FROM quotation  where id=?"
      this.storage.executeSql(sql, [id]).then((data) => {
        //KEY AUTOINCREMENT,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT, status TEX
        let quotation = { customerName: String, customerAddress: String, mobileNo: String, alternateMobileNo: String, emailId: String, venue: String, occasion: String, occasionDateTime: String, platesNumber: String, plateAmount: String, additionalCost: String, total: String, message: String, status: String };
        if (data.rows.length > 0) {

          quotation = data.rows.item(0)

          resolve(quotation)
        }
        else {
          resolve(quotation)
        }

      },
        err => {
          console.log(err)
        })


    })
  }

  /**
   * getQuotationLisrDb
   */
   getQuotationListDb(val, searchValue = '', sparam = '', limit = 10, Offset = 0) {
    return new Promise<{}[]>((resolve, reject) => {

      let sql: string;
      if (val == 'all') {
        console.log("enter 2")
        //quotation ON quotationItems.quotationId = quotation.id
        /// packages (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT)"
        // sql = 'select * from getCustomers';
        sql = "SELECT quotation.*,packages.name as packageName FROM quotation left join  packages ON  quotation.main_id=packages.main_id ORDER BY quotation.id Desc LIMIT " + limit + " OFFSET " + Offset
        this.storage.executeSql(sql, []).then((data) => {
          //,quotation_main_id INTEGER,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT,createdDate TEXT, status TEXT)
          let quotation = [{ customerName: String, customerAddress: String, mobileNo: String, alternateMobileNo: String, emailId: String, venue: String, occasion: String, occasionDateTime: String, platesNumber: String, plateAmount: String, additionalCost: String, total: String, message: String, status: String }];
          quotation = []
          if (data.rows.length > 0) {
            for (let i = 0; i < data.rows.length; i++) {
              quotation[i] = data.rows.item(i)
            }
            resolve(quotation)

            console.log("final quotation " + JSON.stringify(quotation))
          }
          else {
            resolve(quotation)
          }

        },
          err => {
            console.log(err)
          })
      }
      else {
        //      this.storage.executeSql("CREATE TABLE IF NOT EXISTS getCustomers (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,mobileNo TEXT,alternateMobileNo TEXT,createdDate TEXT,updatedDate TEXT,address Text,email TEXT,status TEXT)", {}).then((data) => {
        console.log("enter 3")

        sql = "select quotation.*,packages.name as packageName FROM quotation left join  packages ON  quotation.main_id=packages.main_id WHERE " + sparam + " LIKE ? "
        this.storage.executeSql(sql, [searchValue.toLowerCase() + '%']).then((data) => {
          //KEY AUTOINCREMENT,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT, status TEX
          let quotation = [{ customerName: String, customerAddress: String, mobileNo: String, alternateMobileNo: String, emailId: String, venue: String, occasion: String, occasionDateTime: String, platesNumber: String, plateAmount: String, additionalCost: String, total: String, message: String, status: String }];
          quotation = []
          if (data.rows.length > 0) {
            for (let i = 0; i < data.rows.length; i++) {
              quotation[i] = data.rows.item(i)
            }
            resolve(quotation)
          }
          else {
            resolve(quotation)
          }

        },
          err => {
            console.log(err)
          })
        // sql = "SELECT * FROM pos_shops  AND status='1'";        
      }
      console.log(sql)




    })
  }



  getCustomerbySearchDb(sparam, searchValue) {
    return new Promise<{}[]>((resolve, reject) => {


      let customers = [{ "main_id": String, "name": String, "mobileNo": String, "alternateMobileNo": String, "email": String, "createdDate": String, "updatedDate": String, "status": String, "address": String }];

      this.storage.executeSql("SELECT * FROM getCustomers WHERE " + sparam + "  LIKE ? AND status='1'", [searchValue.toLowerCase() + '%']).then((data) => {
        console.log("enter 1")
        console.log(JSON.stringify(data.rows.item(0)));
        //(id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,mobileNo TEXT,alternateMobileNo TEXT,createdDate TEXT,updatedDate TEXT,address Text,email TEXT,status TEXT)"
        let len = data.rows.length
        console.log("enter 0 " + len)

        if (len > 0) {
          console.log("enter 1")

          for (let i = 0; i < len; i++) {
            customers[i] = (data.rows.item(i))

          }
          resolve(customers)
        }
        else {
          console.log("enter 2")
          customers = []
          resolve(customers)
        }
      })

    })
  }

  getCustomerListDb(val, searchValue = '', sparam = '', limit = 10, Offset = 0) {
    return new Promise<{}[]>((resolve, reject) => {
      let sql: string;
      if (val == 'single') {
        console.log("enter 1")

        sql = 'select * from getCustomers where mobileNo=' + searchValue;

      }
      else if (val == 'all') {
        console.log("enter 2")

        // sql = 'select * from getCustomers';
        sql = "SELECT * FROM getCustomers WHERE status=1 ORDER BY id DESC LIMIT " + limit + " OFFSET " + Offset
      }
      else {
        //      this.storage.executeSql("CREATE TABLE IF NOT EXISTS getCustomers (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,mobileNo TEXT,alternateMobileNo TEXT,createdDate TEXT,updatedDate TEXT,address Text,email TEXT,status TEXT)", {}).then((data) => {
        console.log("enter 3")

        sql = "select * from getCustomers WHERE " + sparam + "  LIKE " + searchValue.toLowerCase() + '%';
        // sql = "SELECT * FROM pos_shops  AND status='1'";        
      }
      console.log(sql)

      let customers = [{ "main_id": String, "name": String, "mobileNo": String, "alternateMobileNo": String, "email": String, "createdDate": String, "updatedDate": String, "status": String, "address": String }];

      this.storage.executeSql(sql, []).then((data) => {
        console.log("enter 1")
        console.log(JSON.stringify(data.rows.item(0)));
        //(id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,mobileNo TEXT,alternateMobileNo TEXT,createdDate TEXT,updatedDate TEXT,address Text,email TEXT,status TEXT)"
        let len = data.rows.length
        console.log("enter 0 " + len)

        if (len > 0) {
          console.log("enter 1")

          for (let i = 0; i < len; i++) {
            customers[i] = (data.rows.item(i))

          }
          resolve(customers)
        }
        else {
          console.log("enter 2")
          customers = []
          resolve(customers)
        }
      })

    })
  }


  /**
   * getItems
   */
  public getItems(id) {
    return new Promise<{}[]>((resolve, reject) => {
      //this.storage.executeSql("CREATE TABLE IF NOT EXISTS quotation (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT, status TEXT)", {}).then((data) => {
      //quotationItems (id INTEGER PRIMARY KEY AUTOINCREMENT,quotationId TEXT,itemId TEXT,itemName TEXT )"
      // this.storage.executeSql("SELECT quotationItems.itemName as name,quotationItems.itemId as main_id,quotation.packageName  FROM   quotationItems LEFT JOIN quotation ON quotationItems.quotationId = quotation.id where quotationItems.quotationId=?", [id]).then((data) => {
      //packages (id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT)", {}).then((data) => {
      this.storage.executeSql("SELECT quotationItems.id , quotationItems.itemName as name,quotationItems.itemId as main_id,packages.name as packageName  FROM   quotationItems  JOIN quotation ON quotationItems.quotationId = quotation.id   JOIN packages  ON quotation.main_id=packages.main_id  where quotationItems.quotationId=? order by quotationItems.id", [id]).then((data) => {
        let len = data.rows.length
        console.log("enter 0 " + id)
        let items = []
        if (len > 0) {
          console.log("enter 1")

          for (let i = 0; i < len; i++) {
            items[i] = (data.rows.item(i))

          }
          resolve(items)
        }
        else {
          console.log("enter 2")
          items = []
          resolve(items)
        }


      },
        err => {
          console.log(err)
        })

    })
  }


  getReceiptsData(id = 0, serverid = 0) {
    return new Promise<{}[]>((resolve, reject) => {
      this.storage.executeSql("SELECT receipts.*,quotation.occasion from receipts left join quotation ON receipts.quotationId=quotation.quotation_main_id where localquotationId=? OR quotationId=?", [id, serverid]).then((data) => {
        let len = data.rows.length
        let receipts = []
        if (len > 0) {
          console.log("enter 1")
          for (let i = 0; i < len; i++) {
            receipts[i] = (data.rows.item(i))
          }
          resolve(receipts);
        }
        else {
          console.log("enter 2")
          receipts = []
          resolve(receipts)
        }


      },
        err => {
          console.log(err)
        })


    })
  }

  /**
   * saveReceiptsDataDb
   */
  public saveReceiptsDataDb(receiptFormData, quotationData, myDate) {
    return new Promise<boolean>((resolve, reject) => {
      //                    // CREATE TABLE IF NOT EXISTS receipts (receiptNo INTEGER PRIMARY KEY AUTOINCREMENT,quotationId TEXT,localquotationId TEXT,receiptDate TEXT ,customerName TEXT,amount TEXT,paymentMode TEXT,chequeDate TEXT,amountWords TEXT,paymentType Text,addedBy TEXT,chequeNo TEXT ,serverReceipt TEXT, isSync TEXT)"
      this.storage.executeSql("INSERT INTO receipts VALUES(null,?,?,?,?,?,?,?,?,?,?,?,?,?)", [quotationData.quotation_main_id ? quotationData.quotation_main_id : 0, quotationData.id, myDate, receiptFormData.name, receiptFormData.paymentAmountfig, receiptFormData.paymentMode, receiptFormData.chequeDt, receiptFormData.paymentAmount, receiptFormData.paymentType, this.userid, receiptFormData.chequeNo, 0, false]).then((data) => {

        console.log(data);
        console.log("data inserted in receipt")
        resolve(true)

      }, err => {
        console.log(err);
        resolve(false)
      })


    })
  }



  /**
   * editQuotationDb
   */
  public editQuotationDb(quotation, selectedpackage, itemsdata, finaldata, quotationId, quotationMainId) {
    return new Promise<boolean>((resolve, reject) => {
      //id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT, status TEXT)
      //quotationId TEXT,itemId TEXT
      //localid INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,main_id TEXT
      //"CREATE TABLE IF NOT EXISTS tempEditquotation (localid INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT, status TEXT)
      console.log(quotationId)
      console.log(quotationMainId)
      console.log(quotation)
      console.log(selectedpackage)
      console.log(itemsdata)
      console.log(finaldata)
      
      if (quotationMainId > 0) {
        this.storage.executeSql("INSERT INTO tempEditquotation VALUES(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [quotationMainId, selectedpackage.id, quotation.customerId, quotation.name, quotation.address, quotation.mobile, quotation.alternateMobileNo, quotation.email, quotation.venue, quotation.occasion, quotation.occasionDt, finaldata.plates, finaldata.perPlateAmount, finaldata.additionalAmount, finaldata.finalAmount, finaldata.comments, selectedpackage.name,  "pending"]).then((data) => {
          console.log(data)
        }, err => {
          console.log(err)
        })
      }
      this.saveCustomerLocalDb(quotation, 0).then((data) => {
        console.log("customer saved")
      })
      this.storage.executeSql("Update  quotation SET main_id =?,customerId =?,customerName =?,customerAddress =?,mobileNo =?,alternateMobileNo =?,emailId =?,venue =?,occasion =?,occasionDateTime =?,platesNumber =?,plateAmount =?,additionalCost =?,total =?, message =?,packageName =?,createdDate=? Where id=?", [selectedpackage.id, quotation.customerId, quotation.name, quotation.address, quotation.mobile, quotation.alternateMobileNo, quotation.email, quotation.venue, quotation.occasion, quotation.occasionDt, finaldata.plates, finaldata.perPlateAmount, finaldata.additionalAmount, finaldata.finalAmount, finaldata.comments, selectedpackage.name,new Date().toLocaleDateString("en-US"), quotationId]).then((data) => {
        console.log(data);
        console.log("enter 1 updated quotation")
        this.deleteAllFromTableConditional('quotationItems', 'quotationId', quotationId).then((datadelted) => {
          console.log("deleted " + JSON.stringify(datadelted))
          let index = itemsdata.length;
          //id INTEGER PRIMARY KEY AUTOINCREMENT,quotation_main_id TEXT,quotationId TEXT,itemId TEXT,itemName TEXT 
          itemsdata.forEach(element => {
            console.log("daata saved in edit quotation " + JSON.stringify(element))
            this.storage.executeSql("INSERT INTO quotationItems VALUES(null,?,?,?,?)", [quotationMainId ? quotationMainId : 0, quotationId, element.main_id, element.name]).then((data) => {
              console.log(data)
              index--;
              if (index == 0) {
                resolve(true)
              }
            },
              err => {
                console.log(err)
                if (index == 0) {
                  resolve(true)

                }
              });

          }, err => {
            console.log(err)
          })


        }, err => {
          console.log(err)
        })
      }, err => {
        console.log(err)
      })

    })
  }


  /**
   * saveQuotationDb
   */
  public saveQuotationDb(quotation, selectedpackage, itemsdata, finaldata) {
    return new Promise<boolean>((resolve, reject) => {
      //id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT, status TEXT)
      //(id INTEGER PRIMARY KEY AUTOINCREMENT,quotation_main_id TEXT,quotationId TEXT,itemId TEXT,itemName TEXT 
      //quotation (id INTEGER PRIMARY KEY AUTOINCREMENT,quotation_main_id INTEGER,main_id TEXT,customerId TEXT,customerName TEXT,customerAddress TEXT,mobileNo TEXT,alternateMobileNo TEXT,emailId Text,venue TEXT,occasion TEXT,occasionDateTime TEXT,platesNumber TEXT,plateAmount Text,additionalCost Text,total Text, message TEXT,packageName TEXT,createdDate TEXT, status TEXT)"
      console.log(quotation)
      console.log(selectedpackage)
      console.log(itemsdata)
      console.log(finaldata)
      this.storage.executeSql("INSERT INTO quotation VALUES(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [0, selectedpackage.id, quotation.customerId, quotation.name, quotation.address, quotation.mobile, quotation.alternateMobileNo, quotation.email, quotation.venue, quotation.occasion, quotation.occasionDt, finaldata.plates, finaldata.perPlateAmount, finaldata.additionalAmount, finaldata.finalAmount, finaldata.comments, selectedpackage.name,new Date().toLocaleDateString("en-US"), "pending"]).then((data) => {
        let insertId = data.insertId
        this.saveCustomerLocalDb(quotation, insertId).then((data) => {
          console.log("customer saved")
        })
        console.log(data);
        console.log("enter 1")
        let index = itemsdata.length;
        itemsdata.forEach(element => {
          this.storage.executeSql("INSERT INTO quotationItems VALUES(null,?,?,?,?)", [0, data.insertId, element.main_id, element.name]).then((data) => {
            console.log(data)
            index--;
            if (index == 0) {
              resolve(true)
            }
          },
            err => {
              console.log(err)
              if (index == 0) {
                resolve(true)
              }
            });
        }, err => {
          console.log(err)
        })
      }, err => {
        console.log(err)
      })

    })
  }
}
