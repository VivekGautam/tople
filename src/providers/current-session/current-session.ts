import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

/*
  Generated class for the CurrentSessionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CurrentSessionProvider {
  currentData: any;
  PackageDataItem = [];
  selectedPackage: any;
  customItem: any;
  constructor(public http: Http) {
    console.log('Hello CurrentSessionProvider Provider');
  }


  setCurrentquotationData(data) {
    this.currentData = data
  }


  getCurrentQuotationData() {
    return this.currentData;
  }



  setCurrentCustomItem(data) {
    this.customItem = data
  }


  getCurrentCustomItem() {
    return this.currentData;
  }

  editCurrentData(val) {
    this.currentData.Menu.push(val)
    console.log(this.currentData)
  }

  setSelectedPackage(data) {
    this.selectedPackage = data
  }



  getSelectedPackage() {
    return this.selectedPackage
  }


  setViewPackageDataItem(data) {
    this.PackageDataItem = data
  }

  editPackageDataItem(val) {
    this.PackageDataItem.push(val)
  }
  getViewPackageDataItem() {
    return this.PackageDataItem
  }
}
