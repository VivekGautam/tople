import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { MenuPage } from '../pages/menu/menu';
import { BrochurePage } from '../pages/brochure/brochure';
import { QuotationPage } from '../pages/quotation/quotation';
import { ListQuotationPage } from '../pages/list-quotation/list-quotation';
import { CustomersPage } from '../pages/customers/customers';
import { DatabaseProvider } from '../providers/database/database';
import { PackagesPage } from '../pages/packages/packages';
import { LoaderUtil } from '../providers/loader-util';
import { BackgroundMode } from '@ionic-native/background-mode';
import { ModelpagePage } from '../pages/modelpage/modelpage';
import { CurrentSessionProvider } from '../providers/current-session/current-session';

import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  @ViewChild(Nav) navCtrl: Nav;
  isBackgroundActive: boolean = false;
  lastTimeOutId = -1;

  pages: Array<{ title: string, icon: string, component: any }>;

  constructor(public currentSession: CurrentSessionProvider, public backgroundMode: BackgroundMode, public modalcontroller: ModalController, public loader: LoaderUtil, public database: DatabaseProvider, platform: Platform, public menu: MenuController, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      this.backgroundFuncDef();
      this.menu.swipeEnable(false)
      this.pages = [
        { title: 'Home', icon: 'home', component: HomePage },
        { title: 'Customers', icon: 'home', component: CustomersPage },
        { title: 'Add Quotation', icon: 'home', component: QuotationPage },
        { title: 'Quotations', icon: 'home', component: ListQuotationPage },
        { title: 'Packages', icon: 'home', component: PackagesPage },
        { title: 'Menu', icon: 'home', component: MenuPage },
        { title: 'Brochure', icon: 'home', component: BrochurePage },
        { title: 'Logout', icon: 'home', component: BrochurePage },

      ];

      this.database.isReady(() => {
        this.database.setMainCallBack((userType) => {
          console.log("enter 1")
        })

        this.database.isSignedIn().then(data => {
          console.log("enter 2")

          if (data != null) {

            this.database.serverSyncStaticData().then(() => {
              this.navCtrl.setRoot(HomePage);
              splashScreen.hide();

            })
            console.log("hide the splash0")
          }
          else {
            this.navCtrl.setRoot(LoginPage);
            console.log("hide the splash1")
            splashScreen.hide();
          }
        })

      })
    });
  }

  backgroundFuncDef() {
    this.backgroundMode.setDefaults({ text: 'Have a Nice Day.' });
    // Enable background mode
    this.backgroundMode.enable();

    this.backgroundMode.on('activate').subscribe(() => {
      console.log("in background on")
      this.isBackgroundActive = true;
      // Splashscreen.show();
      this.database.isSignedIn().then(data => {
        console.log("database.isSignedIn().then..")
        if (data != null) {
          console.log("syncing dynamic data in background.")
          this.database.serverSyncDynamicData().then(() => {
            //   // Splashscreen.hide();
          }, err => {
            //   // Splashscreen.hide();
          })
        } else {
          console.log("database.isSignedIn().then.data false.")
          // this.rootPage = LoginPage;
          // Splashscreen.hide();

        }
      }, err => {
        console.log("database.isSignedIn().err.")
        console.log(err)

      })
      this.backgroundfunc.call(this);
    })
    this.backgroundMode.on('deactivate').subscribe(() => {
      console.log("in background off")
      this.isBackgroundActive = false;
      this.backgroundfunc.call(this);

    })

    this.backgroundfunc.call(this);

  }

  backgroundfunc = () => {
    console.log("syncing on 11")
    if (this.lastTimeOutId != -1) {
      clearTimeout(this.lastTimeOutId);
    }

    this.lastTimeOutId = setTimeout(() => {
      // Modify the currently displayed notification
      console.log('before db ready');
      this.datasync(this.isBackgroundActive);

      this.backgroundMode.configure({
        text: 'Data Syncing.'
      });

      this.backgroundfunc.call(this);

    }, 1500000);
  }



  datasync(inBackground: boolean) {
    this.database.isReady(() => {
      console.log("enter 111")
      this.database.serverSyncDynamicData().then(() => {
        console.log("dynamic sync done")
        this.backgroundMode.configure({
          text: ('Last synced at ' + new Date().toLocaleString())
        });
      })


    });
  }
  openPage(p) {
    if (p.title == 'Menu') {
      this.navCtrl.setRoot(p.component, {
        navOption: false
      })
    }
    // else if (p.title == 'Add Quotation') {
    //   const profileModal = this.modalcontroller.create(ModelpagePage, { custom: true });
    //   profileModal.onDidDismiss(data => {
    //     let dataselected = [];
    //     dataselected = data;
    //     console.log("enter in dismisallllll");
    //     console.log(data);

    //     this.currentSession.customItem = dataselected
    //   })
    //   profileModal.present();
    // }
    else if (p.title == 'Logout') {
      this.loader.showLoader()
      this.database.Logout().then((data) => {
        this.navCtrl.setRoot(LoginPage);
        this.loader.hideLoader()
      }, err => {
        console.log(err);
        this.navCtrl.setRoot(LoginPage)
        this.loader.hideLoader()

      })
    }
    else {
      this.navCtrl.setRoot(p.component)
    }

  }
}
