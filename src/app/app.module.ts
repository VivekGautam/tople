import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { PackagesPage } from '../pages/packages/packages';
import { CustomerDetailPage } from '../pages/customer-detail/customer-detail';
import { QuotationPage } from '../pages/quotation/quotation';
import { EmailValidator } from '../validators/email';
import { StandardPage } from '../pages/standard/standard';
import { ListQuotationPage } from '../pages/list-quotation/list-quotation';
import { ModelpagePage } from '../pages/modelpage/modelpage';
import { CustomersPage } from '../pages/customers/customers';
import { FinalOrderPage } from '../pages/final-order/final-order';
import { MenuPage } from '../pages/menu/menu';
import { BrochurePage } from '../pages/brochure/brochure';
import { ReceiptsPage } from '../pages/receipts/receipts';
import { AddReceiptsPage } from '../pages/add-receipts/add-receipts';
import { LoaderUtil } from '../providers/loader-util';
import { AlertUtil } from '../providers/alert-util';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { CurrentSessionProvider } from '../providers/current-session/current-session';
import { DatabaseProvider } from '../providers/database/database';
import { ConstantProvider } from '../providers/constant/constant';
import { SQLite } from '@ionic-native/sqlite';
import { BackgroundMode } from '@ionic-native/background-mode';
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    LoginPage,
    HomePage,
    MyApp,
    QuotationPage,
    StandardPage,
    FinalOrderPage,
    MenuPage,
    BrochurePage,
    ListQuotationPage,
    ModelpagePage,
    CustomersPage,
    PackagesPage,
    CustomerDetailPage,
    ReceiptsPage,
    AddReceiptsPage
  ],
  imports: [
    BrowserModule, HttpModule,    
    
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    LoginPage,
    HomePage,
    MyApp,
    QuotationPage,
    StandardPage,
    FinalOrderPage,
    MenuPage,
    BrochurePage,
    ListQuotationPage,
    ModelpagePage,
    CustomersPage,
    PackagesPage,
    CustomerDetailPage,
    ReceiptsPage,
    AddReceiptsPage
  ],
  providers: [
    StatusBar,
    SplashScreen, LoaderUtil,SQLite,BackgroundMode,
    EmailValidator, AlertUtil, CurrentSessionProvider,SocialSharing,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    DatabaseProvider,
    ConstantProvider,
  ]
})
export class AppModule { }
