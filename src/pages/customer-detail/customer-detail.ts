import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CustomerDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-customer-detail',
  templateUrl: 'customer-detail.html',
})
export class CustomerDetailPage {
  customerDetail = { "main_id": String, "name": String, "mobileNo": String, "alternateMobileNo": String, "email": String, "createdDate": String, "updatedDate": String, "status": String, "address": String }
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.customerDetail = this.navParams.get('detail');
    console.log(this.customerDetail)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomerDetailPage');
  }

}
