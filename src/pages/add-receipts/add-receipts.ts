import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { DatabaseProvider } from '../../providers/database/database';
import { AlertUtil } from '../../providers/alert-util';
import { LoaderUtil } from '../../providers/loader-util';
import { ReceiptsPage } from '../../pages/receipts/receipts';

/**
 * Generated class for the AddReceiptsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-receipts',
  templateUrl: 'add-receipts.html',
})
export class AddReceiptsPage {
  receiptForm: any;
  submitAttempt: boolean = false;
  quotationData = {};
  paymentType = ['Full', 'Part', 'Advance payment'];
  paymentMode = ['Cash', 'Cheque'];
  myDate: string = new Date().toISOString();
  myDateCheque: string = '';
  checkFieldShow: boolean = false;
  errorChequeNo: Boolean = false;
  errorChequeDt: Boolean = false
  constructor(public navCtrl: NavController, public navParams: NavParams, public frmbuilder: FormBuilder, public database: DatabaseProvider, public alert: AlertUtil, public loader: LoaderUtil) {
    this.receiptForm = frmbuilder.group({
      receiptDt: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      name: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      paymentAmount: [''],
      paymentAmountfig: [''],
      paymentMode: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      chequeNo: [''],
      chequeDt: [''],
      paymentType: ['', Validators.compose([Validators.minLength(3), Validators.required])],
    })

    console.log(this.navParams.get('quotationdata'))
    this.quotationData = this.navParams.get('quotationdata')
    this.receiptForm.name = this.quotationData['customerName'];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddReceiptsPage');
  }


  save() {
    this.submitAttempt = true;

    if (this.checkFieldShow) {
      if (!this.receiptForm.valid) {
        console.log(" error")
      }
      if (this.myDateCheque.length == 0) {
        this.errorChequeDt = true;
        if (this.receiptForm.value.chequeNo.length == 0) {
          this.errorChequeNo = true;

        }
        else {
          this.errorChequeNo = false;

        }
        return;
      }
      else {
        this.errorChequeNo = false;

      }
      if (this.receiptForm.value.chequeNo.length == 0) {
        this.errorChequeNo = true;
        return;

      }
      else {
        this.errorChequeNo = false;

      }

    }

    if (!this.receiptForm.valid) {
      console.log(" error")
    }

    else {
      console.log("current data " + JSON.stringify(this.receiptForm.value))
      // this.currentSession.setCurrentData(this.quotationForm.value.packages)
      this.loader.showLoader();
      this.database.saveReceiptsDataDb(this.receiptForm.value, this.quotationData,this.myDate).then((data) => {
        this.loader.hideLoader();
        this.alert.showOkAlert("Succesfull", "Receipt saved.");
        this.navCtrl.push(ReceiptsPage,{
          quotationId:  this.quotationData['id']
          
        });
       

        this.receiptForm.receiptDt="";
        this.receiptForm.name ="";
        this.receiptForm.paymentAmount ="";
        this.receiptForm.paymentAmountfig ="";
        this.receiptForm.chequeNo ="";
        this.receiptForm.chequeDt ="";
        this.receiptForm.paymentType ="";
        this.receiptForm.paymentMode ="";
       
        this.database.syncReceiptsApi().then((data) => {

        })
      },
        err => {
          console.log(err);
          this.alert.showOkAlert("Error", "error in saving receipt.")

        })

    }
  }


  selectedPaymentMode() {

    if(this.receiptForm.value.paymentMode=='Cash'){
      this.checkFieldShow=false;
      console.log("enter 1")
    }
    else{
      this.checkFieldShow=true;
      console.log("enter 2")
      
    }
  }
}
