import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { FinalOrderPage } from '../../pages/final-order/final-order';
import { CurrentSessionProvider } from '../../providers/current-session/current-session';
import { ModelpagePage } from '../../pages/modelpage/modelpage';
import { DatabaseProvider } from '../../providers/database/database';
import { LoaderUtil } from '../../providers/loader-util';
import { AlertUtil } from '../../providers/alert-util';

/**
 * Generated class for the StandardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-standard',
  templateUrl: 'standard.html',
  preserveWhitespaces: false

})
export class StandardPage {
  selectedMenu = [];
  selectedPackage: number = 0;
  selectedClass = [true];
  customItems = [];
  backUpCustomeItems = []
  standardMenu = [];

  constructor(public alert: AlertUtil, public loader: LoaderUtil, public database: DatabaseProvider, public currentSession: CurrentSessionProvider, public modalcontroller: ModalController, public navCtrl: NavController, public navParams: NavParams) {
    this.loader.showLoader()
    this.database.getpackagesItemsDb().then((data) => {
      console.log(JSON.stringify(data))
      this.standardMenu = data;
      this.selectedPackage = this.navParams.get('package');
      // this.selectedMenu =  this.navParams.get('edit') ? this.getItems()  : this.standardMenu[this.selectedPackage].Menu;

      if (this.navParams.get('edit')) {
        this.getItems()
      }
      else {
        console.log("enter in 2 edit false")
        this.selectedMenu = this.standardMenu[this.selectedPackage].Menu;
        this.loader.hideLoader()

      }
      console.log("first time data")
      console.log(this.navParams.get('edit'))
      console.log(this.navParams.get('quotationData'))
      console.log(this.standardMenu);
      console.log(this.selectedPackage)
      console.log(this.selectedMenu)



    })
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad StandardPage');

  }
  getItems() {
    console.log("get itemmmmmmmmmm")
    let quotationId = this.navParams.get('quotationData')
    this.database.getItems(quotationId.id).then((data) => {
      this.selectedMenu = data;
      console.log("selected menu");
      console.log(this.selectedMenu)
      this.loader.hideLoader()

    })
  }
  navToAddItems() {
    console.log(" enter")
    const profileModal = this.modalcontroller.create(ModelpagePage, { custom: false });
    profileModal.onDidDismiss(data => {

      this.customItems = data;
      console.log("enter in dismisallllll menu change");
      console.log(data);
      for (let i = 0; i < this.customItems.length; i++) {
        this.backUpCustomeItems.push(this.customItems[i])
      }
      for (let i = 0; i < this.customItems.length; i++) {
        this.selectedMenu.unshift(this.customItems[i])
      }

    })
    profileModal.present();

  }
  trackByFn(index, h) {
    return index;
  }

  menuFunc() {
      console.log("menu func changed")
      console.log(this.backUpCustomeItems);
      this.selectedMenu = this.standardMenu[this.selectedPackage].Menu;
      if (this.backUpCustomeItems != undefined) {
        for (let i = 0; i < this.backUpCustomeItems.length; i++) {
          this.selectedMenu.unshift(this.backUpCustomeItems[i])
        }
      }
    
  }
  navToNextPage() {
    console.log("enter")
    this.currentSession.setSelectedPackage({ "id": this.standardMenu[this.selectedPackage].packageId, "name": this.standardMenu[this.selectedPackage].name });
    this.currentSession.setViewPackageDataItem(this.selectedMenu)
    console.log("selected menu")
    console.log(this.selectedMenu)
    if (this.selectedMenu.length == 0) {
      this.alert.showOkAlert("Information", "Please Select Items for adding order");
      return;
    }
    this.navCtrl.push(FinalOrderPage, {
      quotationData: this.navParams.get('quotationData'),
      edit: (this.navParams.get('edit'))
    })
  }




  lostFocus(i) {
    this.selectedClass[i] = true;

  }

  getFocus(i) {
    this.selectedClass[i] = false;

  }

  removeItem(val) {
    this.selectedMenu.splice(val, 1);;

  }

}
