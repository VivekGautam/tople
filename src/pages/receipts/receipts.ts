import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { LoaderUtil } from '../../providers/loader-util';
declare var cordova: any;    //global;

/**
 * Generated class for the ReceiptsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-receipts',
  templateUrl: 'receipts.html',
})
export class ReceiptsPage {
  receipts = [];
  receiptsfound: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public databse: DatabaseProvider, public loader: LoaderUtil) {
    this.loader.showLoader();
    console.log(this.navParams.get('quotationId'));
    console.log(this.navParams.get('serverId'));
    
    this.databse.getReceiptsData(this.navParams.get('quotationId'),this.navParams.get('serverId')).then((data) => {
      this.receipts = data;
      this.receiptsfound = this.receipts.length > 0 ? false : true
      for (let i = 0; i < this.receipts.length; i++) {
        let dt: string = this.receipts[i]['receiptDate']
        this.receipts[i]['ReceiptDt'] = this.convertDate(dt.split('T')[0])
        dt = this.receipts[i]['chequeDate']
        this.receipts[i]['ChequeDt'] = this.convertDate(dt.split('T')[0])

      }

      console.log(JSON.stringify(this.receipts))
      this.loader.hideLoader()
    },
      err => {
        console.log(err);
        this.loader.hideLoader()

      })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiptsPage');
  }


  convertDate(date) {
    //linux_date = "2001-01-02"
    var arrDate = date.split("-");
    return arrDate[2] + "/" + arrDate[1] + "/" + arrDate[0];
  }
  // <div style='margin-bottom:25px'>  <table style='width:100%'>  <col width='230'> <col width='370'> <col width='70'> <col width='280'> <tr >        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>Quotation Id.</h3>        </td>        <td >          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> "  + val.quotationId + "</h3>        </td>   <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>Occasion</h3>        </td>        <td >          <h3 style='border-bottom:solid 1px;width:100%;text-align:center;padding:0px;margin:0px;'>" + val.occasion + "</h3>        </td>     </tr>  </table></div>

  printPdf(val) {
    console.log(JSON.stringify(val))
    let paymentType = val.paymentMode == 'cheque' ? val.chequeNo : val.amount;
    let dated = val.paymentMode == 'cheque' ? val.ChequeDt : val.ReceiptDt
    let pdfData = "<html><div style='margin-bottom:50px;'>     <br>     <h1 style='text-align:center;margin:0px;padding:0px;'>TOPLE CATERERS    </h1>      <h3 style='text-align:center;padding:0px;margin:0px;margin-left:6px'>Bicholim-Goa</h3>    <h3 style='text-align:center;padding:0px;margin:0px;margin-left:6px'>RECEIPT</h3>                <table style='width:100%'>  <col width='200'> <col width='650'><col width='190'>     <tr style='margin-bottom:5px'>        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>No.:" + val.receiptNo + "</h3>        </td>        <td>          <h3 style='width:100%;text-align:right;padding:0px;margin:0px;'>Date: </h3>        </td> <td>          <h3 style='border-bottom:solid 1px;width:100%;text-align:center;padding:0px;margin:0px;'>" + val.ReceiptDt + "</h3>        </td>     </tr>   </table>  </div>                                                                   <div style='margin-bottom:25px'>  <table style='width:100%'>  <col width='600'> <col width='580'>  <tr >        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>Received with thanks from Mr./Ms.:</h3>        </td>        <td >          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> " + val.customerName + "</h3>        </td>      </tr>  </table></div>  <div style='margin-bottom:25px'> <table style='width:100%'>  <col width='280'> <col width='650'>  <tr >        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>the sum of of Rupees</h3>        </td>        <td >          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> " + val.amountWords + "</h3>        </td>      </tr>  </table></div><div style='margin-bottom:25px'>  <table style='width:100%'>  <col width='340'> <col width='370'> <col width='70'> <col width='180'> <tr >        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>by Cash / Cheque no.</h3>        </td>        <td >          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> "  + paymentType + "</h3>        </td>   <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>Dated</h3>        </td>        <td >          <h3 style='border-bottom:solid 1px;width:100%;text-align:center;padding:0px;margin:0px;'>" + dated + "</h3>        </td>     </tr>  </table></div><div style='margin-bottom:25px'> <table style='width:100%'>  <col width='1130'> <col width='350'>  <tr >        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>towards Full / Part / Advance Payment of Quotation Id: </h3>        </td>        <td >          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'>" + val.quotationId + "</h3>        </td>      </tr> </table></div> <div style='margin-bottom:25px'> <table >  <col width='160'> <col width='150'>  <tr >   <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'> and occasion:  </h3>        </td>        <td >        <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> " + val.occasion +"</h3>        </td>      </tr> </table></div>                    <div> <table>  <col width='30'> <col width='70'>  <tr>        <td>          <h3 style='text-align:left;padding:0px;margin:0px;'>Rs. </h3>        </td>        <td>          <h3 style='border-bottom:solid 1px;text-align:left;padding:0px;margin:0px;'> 5000</h3>        </td>      </tr>  </table></div> <div style='margin-bottom:25px'> <table> <col width='600'>  <col width='220'>   <tr >    <td> </td>     <td>          <h3 style='border-top:solid 1px;text-align:right;padding:0px;margin:0px; width:100%'>Receiver's Signature</h3>        </td>  </tr>  </table></div> </html>  ";

    let htmlData = pdfData
    console.log(" html data  " + htmlData)
    cordova.plugins.pdf.htmlToPDF({
      data: htmlData,
      documentSize: "A4",
      landscape: "portrait",
      type: "share"
    },
      ((sucess) => {
        console.log('sucess: ', sucess)
        // let c = this.dataURItoBlob(sucess)
        console.log(" c result");
        console.log(sucess)
      }),
      ((error) => {
        console.log('error: ', error)
        // let c = this.dataURItoBlob(sucess)
        console.log(" c result");
        console.log(error)

      })


    )

  }
}
