import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { HomePage } from '../../pages/home/home';
import { DatabaseProvider } from '../../providers/database/database';
import { LoaderUtil } from '../../providers/loader-util';
import { AlertUtil } from '../../providers/alert-util';


// import { Http, Headers, RequestOptions } from '@angular/http';
// import 'rxjs/add/operator/map';

/**
 * Generated class for the LoginPage page
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  preserveWhitespaces: false
  
})
export class LoginPage {
  public loginForm;
  submitAttempt: boolean = false;
  selectedClass = [true, true];

  constructor(public alert:AlertUtil,public loader:LoaderUtil,public database: DatabaseProvider, public navCtrl: NavController, public navParams: NavParams, public frmbuilder: FormBuilder) {
    this.loginForm = frmbuilder.group({
      username: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(3), Validators.required])]
    })
  }
  loginUser() {
    this.submitAttempt = true;

    if (!this.loginForm.valid) {
      console.log(this.loginForm.value);

    } 
    else {
    
      this.loader.showLoader()
      this.database.signIn(this.loginForm.value.username, this.loginForm.value.password).then((data) => {
        console.log(data);
        if(data){
          this.database.serverSyncStaticData().then(() => {
          this.navCtrl.setRoot(HomePage);
          this.loader.hideLoader()
          })
        }
        else{

          this.loader.hideLoader();
          this.alert.showOkAlert("Error","Wrong username or password")
        }
      

      },
        err => {
          console.log(err);
          this.loader.hideLoader()
          this.alert.showOkAlert("Error","Connect to your internet for login")
          
        })
      console.log("enter ")
    }

   
  }






}
