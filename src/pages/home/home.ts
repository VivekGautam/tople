import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { QuotationPage } from '../../pages/quotation/quotation';
import { PackagesPage } from '../../pages/packages/packages';
import { ListQuotationPage } from '../../pages/list-quotation/list-quotation';
import { CustomersPage } from '../../pages/customers/customers';

/**                    
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  preserveWhitespaces: false
  
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
   
  }


  navToQuotation(){
    this.navCtrl.push(QuotationPage)
  }
                   
  navToMenu(){
    this.navCtrl.push(PackagesPage)
  }

  navToListQutation()
  {

    this.navCtrl.push(ListQuotationPage)

  }

  navToCustomers(){
    this.navCtrl.push(CustomersPage)
    
  }
}
