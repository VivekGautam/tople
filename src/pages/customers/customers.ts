import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { LoaderUtil } from '../../providers/loader-util';
import { FormControl } from '@angular/forms';
// import 'rxjs/add/operator/debounceTime';
import { debounceTime } from 'rxjs/operators/debounceTime';

import { CustomerDetailPage } from '../../pages/customer-detail/customer-detail';

/**
 * Generated class for the CustomersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-customers',
  templateUrl: 'customers.html',
  preserveWhitespaces: false

})
export class CustomersPage {
  searchControl: FormControl;
  customerCount: number;
  @ViewChild('searchTextInput') searchTextInput;
  customers = [];
  search: Array<string>;
  s: any;
  searchParam: string;
  searchTerm: string = '';
  min: boolean = true;
  max: boolean = false;
  totalpages: any;
  countermaxpage: any = 1;
  next: number = 0;
  customerPerPage: number = 5;
  selectedSearchType: string = "text";
  constructor(public loader: LoaderUtil, public database: DatabaseProvider, public navCtrl: NavController, public navParams: NavParams) {
    //(id INTEGER PRIMARY KEY AUTOINCREMENT,main_id TEXT,name TEXT,mobileNo TEXT,alternateMobileNo TEXT,createdDate TEXT,updatedDate TEXT,address Text,email TEXT,status TEXT)"
    this.s = "Name";
    this.search = ['Name', 'Mobile No.'];
    this.searchControl = new FormControl();
    this.refreshData();
    this.customerCountFunc();
  }

  isRefreshing = false;

  refreshData() {
    console.log("refresh " + this.isRefreshing)
    if (this.isRefreshing)
      return;
    this.isRefreshing = true;

    this.loader.showLoader()
    this.database.getCustomerListDb('all', '', '', this.customerPerPage, 0).then((data) => {
      console.log("customer data from db")
      console.log(data);
      this.loader.hideLoader()
      this.customers = data;
      this.isRefreshing = false;
      if (this.customers.length <= 0) {
        if(this.customerCount>0){
          this.refreshData();

        }
      } else {
        this.loader.hideLoader();
      }
      if(this.customers.length>5)
      {
        this.max=false;
        this.min=true;
      }
    }, err => {
      console.log("refreshData 3")
      this.isRefreshing = false;
      this.loader.hideLoader();
    })
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomersPage');
    this.searchControl.valueChanges.pipe(debounceTime(800)).subscribe(search => {
      // this.searchControl.valueChanges.debounceTime(800).subscribe(search => {
      console.log("enter1111")
      this.setFilteredItems();
    });
  }


  optionSelection() {
    console.log("enteerr")
    if (this.s == 'Name') {
      this.selectedSearchType = "text"
    }
    else {
      this.selectedSearchType = "tel"

    }

  }


  setFilteredItems() {

    console.log("entered in filterd item")
    if (this.s == 'Name') {
      this.searchParam = "name"
    }
    else {
      this.searchParam = "mobileNo"
    }
    // this.max = true;
    // this.min = true;
    console.log("length of searchterm")
    console.log(this.searchTerm.length);
    if (this.searchTerm.length > 1) {
      this.loader.showLoader();
      this.database.getCustomerbySearchDb(this.searchParam, this.searchTerm).then((data) => {
        this.customers = data;
        // for(let i=0;i<this.customers.length;i++)
        console.log("enter 3")
        this.max=true;
        this.min=true;
        this.loader.hideLoader();

      }

      );
    }
    else if (this.searchTerm.length == 0) {
      this.refreshData();
      console.log("data")
      this.max=false;
      this.min=true;
    }

  }


  trackByFn(index, h) {
    return index;
  }

  navToCustDetail(val) {
    this.navCtrl.push(CustomerDetailPage, {
      detail: val
    })
  }

  customerCountFunc() {
    this.database.customerCount().then((data) => {
      this.customerCount = data;
      console.log("111111  :" + data);
      if (this.customerCount <= this.customerPerPage) {
        this.max = true;
      }
    });
  }


  loaddata(val) {
    this.loader.showLoader();
    this.totalpages = Math.ceil(this.customerCount / this.customerPerPage);
    console.log(this.customerCount);
    console.log(this.totalpages);
    this.next = val + this.next;
    console.log(this.next + "value");
    console.log("button1 valid or unvalid")
    if (this.next <= 0) {
      this.min = true;
      this.max = false;
      console.log(this.next + "value");
      this.database.getCustomerListDb('all', '', '', this.customerPerPage, this.next).then((data) => {
        this.customers = data;
        this.countermaxpage = this.countermaxpage - 1;
        this.loader.hideLoader();

      }, err => {
        this.loader.hideLoader();

      })
    }
    else if (val > 0) {
      this.countermaxpage = this.countermaxpage + 1;
      this.database.getCustomerListDb('all', '', '', this.customerPerPage, this.next).then((data) => {
        this.customers = data;
        this.loader.hideLoader();
      }, err => {
        this.loader.hideLoader();

      })
      this.min = false;

      if (this.countermaxpage == this.totalpages) {
        this.min = false;
        this.max = true;
        console.log("unable")
        console.log(this.min);
        console.log(this.next + "page");
        console.log(this.countermaxpage);

      }

    }
    else if (val < 0) {
      console.log(this.countermaxpage);
      console.log(val < 10);
      this.countermaxpage = this.countermaxpage - 1;
      this.database.getCustomerListDb('all', '', '', this.customerPerPage, this.next).then((data) => {
        this.customers = data;
        this.max = false;
        this.loader.hideLoader();
      }, err => {
        this.loader.hideLoader();

      });
    }

  }



  onSearchInput() {
    console.log("Enter in 111111")
  }
}
