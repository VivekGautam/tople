import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the BrochurePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-brochure',
  templateUrl: 'brochure.html',
  
  preserveWhitespaces: false
})
export class BrochurePage {
  constructor(public socialSharing: SocialSharing, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BrochurePage');
  }



  shareBorchure()
  {
    console.log("enter in social")
    this.socialSharing.share("Borchure ", '', 'http://toplecaterers.webfillssoftwares.com/sales/public/brochure/brochure.jpg').then((data) => {
      console.log(data);
      console.log("enter 1")
    },
      err => {
        console.log("enter 2")

        console.log(err)
      })
  }
}
