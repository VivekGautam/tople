import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
declare var cordova: any;    //global;
import { QuotationPage } from '../../pages/quotation/quotation';
import { debounceTime } from 'rxjs/operators/debounceTime';
import { FormControl } from '@angular/forms';
import { LoaderUtil } from '../../providers/loader-util';
import { ReceiptsPage } from '../../pages/receipts/receipts';
import { AddReceiptsPage } from '../../pages/add-receipts/add-receipts';

/**
 * Generated class for the ListQuotationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-list-quotation',
  templateUrl: 'list-quotation.html',
})
export class ListQuotationPage {
  search: Array<string>;
  listQuotation = [];
  dataAvail: boolean = false;
  searchControl: FormControl;
  quotationCount: number;
  @ViewChild('searchTextInput') searchTextInput;
  s: any;
  searchParam: string;
  searchTerm: string = '';
  quotationPerPage: number = 5;
  min: boolean = true;
  max: boolean = false;
  countermaxpage: any = 1;
  totalpages: any;

  next: number = 0;
  constructor(public loader: LoaderUtil, public navCtrl: NavController, public navParams: NavParams, public database: DatabaseProvider) {
    this.search = ['Name', 'Contact No.'];
    this.s = "Name";
    this.searchControl = new FormControl();
    this.loader.showLoader()
    this.refreshData();
    this.quotationCountFunc();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListQuotationPage');
    this.searchControl.valueChanges.pipe(debounceTime(800)).subscribe(search => {
      // this.searchControl.valueChanges.debounceTime(800).subscribe(search => {
      console.log("enter1111")
      this.setFilteredItems();
    });
  }

  setFilteredItems() {
    //customerName TEXT,customerAddress TEXT,mobileNo TEXT
    console.log("entered in filterd item")
    if (this.s == 'Name') {
      this.searchParam = "customerName"
    }
    else {
      this.searchParam = "mobileNo"
    }
    // this.max = true;
    // this.min = true;
    console.log("length of searchterm")
    console.log(this.searchTerm.length);
    if (this.searchTerm.length > 1) {

      this.loader.showLoader();
      this.database.getQuotationListDb('search', this.searchTerm, this.searchParam).then((data) => {
        this.listQuotation = data;
        this.dateFormat()
        this.max = true;
        this.min = true;
        this.loader.hideLoader();

      }

      );
    }
    else if (this.searchTerm.length == 0) {
      this.refreshData();

      console.log("data")
    }

  }


  trackByFn(index, h) {
    return index;
  }
  convertDate(date,special_character) {
    //linux_date = "2001-01-02"
    if(special_character=='-'){
      var arrDate = date.split(special_character);
      return arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
    }
    else{
      var arrDate = date.split(special_character);
      return arrDate[1] + "-" +  arrDate[0]+ "-"+arrDate[2];
    }
  
  }


  isRefreshing = false;
  refreshData() {
    if (this.isRefreshing)
      return;
    this.isRefreshing = true;
    this.database.getQuotationListDb('all', '', '', this.quotationPerPage, 0).then((data) => {
      this.listQuotation = data;
      if (this.listQuotation.length > 0) {
        this.dataAvail = true
      }
      this.dateFormat()
      if (this.listQuotation.length > 5) {
        this.max = false;
        this.min = true;
      }
      console.log(JSON.stringify(data))
    })
    this.loader.hideLoader()
  }

  navToAddQuotation() {
    this.navCtrl.setRoot(QuotationPage)
  }


  pdfGenerator(val) {
    console.log(val)
    this.database.getItems(val.id).then((itemsData) => {
      console.log("data items")
      console.log(itemsData)
      let time: string = (val.occasionDateTime.split('T')[1]);
      time = time.substr(0, 5)

      let data: string = "";
      if (itemsData.length > 0) {
        data = "<tr style='border:solid 2px;border-style: thick'><td style='border:solid 2px;border-style: thick'>&nbsp; " + (0 + 1) + "</td><td style='border:solid 2px;border-style: thick'> &nbsp;" + itemsData[0]['name'] + " </td><td style='border:solid 2px;border-style: thick'> &nbsp;" + itemsData[0]['packageName'] + "</td></tr>"
        for (let i = 1; i < itemsData.length; i++) {
          data = data + "<tr  style='border:solid 2px;border-style: thick'><td  style='border:solid 2px;border-style: thick'>&nbsp; " + (i + 1) + "</td><td  style='border:solid 2px;border-style: thick'> &nbsp;" + itemsData[i]['name'] + " </td><td></td></tr>"
        }
      }
      let alertMobileNo: string = val.alternateMobileNo ? val.alternateMobileNo : 'N/a'

      let tableData = data;
      let beforeTable = "<html><div style='border-bottom:solid 2px'>     <br>     <h3 style='text-align:center;margin:0px;padding:0px;margin-left:250px;'>Quotation  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;No." + val.quotation_main_id + " </h3>                 </div>  <div class='row'>   <h1 style='font-size:30px;text-align:left;padding:0px;margin:0px;display:inline'>TOPLE CATERERS  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style='font-size:15px;display:inline;margin-left:80px'> Date:<u>" + val.created_date + "</u> </span> </h1>     </div>        <div class='col-sm-3'> <h3 style='text-align:left;padding:0px;margin:0px;margin-left:6px'>Bicholim-Goa</h3>  <h3 style='text-align:left;padding:0px;margin:0px;margin-left:6px'>MOB.NO.:9158778040,   &nbsp;7875241069&nbsp;/&nbsp;08007671899</h3>  <div style='border:solid 2px;margin-left:6px;margin-right:6px;margin-bottom:20px'>    <table style='width:100%'>  <col width='200'> <col width='580'>     <tr style='margin-bottom:5px'>        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>Name of Party:</h3>        </td>        <td>          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> " + val.customerName + "</h3>        </td>      </tr>   </table>  <table style='width:100%'>    <tr style='margin-bottom:5px'>        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>Address:</h3>        </td>        <td colspan='5'>          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> " + val.customerAddress + "</h3>        </td>      </tr>      <tr>        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>Function:</h3>        </td>        <td>          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> " + val.occasion + "</h3>        </td>        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>Mob.:</h3>        </td>        <td>          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> " + val.mobileNo + "</h3>        </td>        <td>          <h3 style='text-align:left;padding:0px;margin:0px; width:100%'>Mob.:</h3>        </td>        <td>          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;padding:0px;margin:0px;'> " + alertMobileNo + "</h3>        </td>      </tr>     </table> </div ><table style='width:98%; border-collapse: collapse;margin-bottom:20px;margin-left:8px;margin-right:10px;padding:0px' border='1';border-style: thick> <col width='40'> <col width='120'> <col width='120'><tr style='margin-bottom:5px;border:solid 2px;border-style: thick'><td style='border:solid 2px;border-style: thick'><h3 style='text-align:left;padding:0px;margin:0px;'> Sr No.</h3> </td> <td style='border:solid 2px;border-style: thick'> <h3 style='width:100%;text-align:center;width:100%;padding:0px;margin:0px;' > Content</h3>  </td ><td style='border:solid 2px;border-style: thick'> <h3 style='width:100%;text-align:center;width:100%;padding:0px;margin:0px;' > Description</h3>  </td > </tr>";

      let afterTable = "</table><div style='border:solid 2px;margin-left:6px;margin-right:6px;margin-bottom:20px'><table style='width:100%'> <col width='100'> <col width='680'><tr style='margin-bottom:5px'><td><h3 style='text-align:left;padding:0px;margin:0px;'> Venue:</h3> </td> <td> <h3 style='border-bottom:solid 1px;width:100%;text-align:left;width:100%;padding:0px;margin:0px;' > " + val.venue + "</h3>  </td > </tr></table>                                                                            <table style='width:100%'> <col width='150'> <col width='200'><col width='100'> <col width='160'><col width='110'><tr style='margin-bottom:5px'><td><h3 style='text-align:left;padding:0px;margin:0px;'> Date & Time:</h3> </td> <td> <h3 style='border-bottom:solid 1px;width:100%;text-align:left;width:100%;padding:0px;margin:0px;' >" + val.date + " " + time + "</h3>  </td > <td></td><td></td></tr>       </table>       <table style='width:100%'>                                                                    <col width='180'> <col width='120'><col width='350'><tr style='margin-bottom:5px'><td><h3 style='text-align:left;padding:0px;margin:0px;'> Quantity Ordered:</h3> </td> <td> <h3 style='border-bottom:solid 1px;width:100%;text-align:left;width:100%;padding:0px;margin:0px;' > " + val.platesNumber + " &nbsp; Plates</h3>  </td ><td>&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr></table>   <table style='width:100%'> <col width='70'> <col width='110'><col width='220'> <col width='220'><col width='110'><tr><td><h3 style='text-align:left;padding:0px;margin:0px;'> Rate:</h3> </td> <td> <h3 style='border-bottom:solid 1px;width:100%;text-align:left;width:100%;padding:0px;margin:0px;' > " + val.plateAmount + "</h3>  </td > <td></td><td><h3 style='text-align:left;padding:0px;margin:0px;'> Signature of Party:</h3> </td> <td> <h3 style='width:100%;text-align:left;width:100%;padding:0px;margin:0px;' >" + "       " + "</h3>  </td ></tr>       </table>    </div>                                      <div>    <table style='width:100%'>      <tr>        <td>        <h4  style='padding:0px;margin:0px'> Note:</h4> </td>        <td>       <h4 style='padding:0px;margin:0px'>  1) 50% advance should pay against the order.</h4> </td>      </tr> <tr>                  <td></td>          <td>              <h4 style='padding:0px;margin:0px'>  2)  Ice & Fruit carvings will be charge extra.</h4> </td>        </tr> <tr><td>                  </td>            <td>                <h4 style='padding:0px;margin:0px'>  3) Extra plates will be charge.</h4> </td> </tr> </table></div></html> ";


      // <tr>        <td>          <h3 style='text-align:left;padding:0px;margin:0px; '>Qty. Ordered:</h3>              </td>        <td>          <h3 style='border-bottom:solid 1px;width:100%;text-align:left;width:100%'> 150 kg</h3> </td>      </tr> <tr><td><h3 style='text-align:left;padding:0px;margin:0px; ' > Rate:</h3>        </td> <td><h3 style='border-bottom:solid 1px;width:100%;text-align:left;width:100%' > 350 / -</h3>        </td> <td><h3 style='text-align:left;padding:0px;margin:0px; ' > Signature of Party: </h3>        </td> <td>                <h3 style='border-bottom:solid 1px;width:100%;text-align:left;width:100%' > sadjsn / -</h3>        </td>                  </tr>    </table>
      let htmlData = beforeTable + tableData + afterTable;
      console.log(" html data  " + htmlData)
      cordova.plugins.pdf.htmlToPDF({
        data: htmlData,
        documentSize: "A4",
        landscape: "portrait",
        type: "share"
      },
        ((sucess) => {
          console.log('sucess: ', sucess)
          // let c = this.dataURItoBlob(sucess)
          console.log(" c result");
          console.log(sucess)

        }),
        ((error) => {
          console.log('error: ', error)
          // let c = this.dataURItoBlob(sucess)
          console.log(" c result");
          console.log(error)

        })


      )

    })


  }



  loaddata(val) {
    this.loader.showLoader();
    this.totalpages = Math.ceil(this.quotationCount / this.quotationPerPage);
    console.log(this.quotationCount);
    console.log(this.totalpages);
    this.next = val + this.next;
    console.log(this.next + "value");
    console.log("button1 valid or unvalid")
    if (this.next <= 0) {
      this.min = true;
      this.max = false;
      console.log(this.next + "value");
      this.database.getQuotationListDb('all', '', '', this.quotationPerPage, this.next).then((data) => {
        this.listQuotation = data;
        this.dateFormat();
        this.countermaxpage = this.countermaxpage - 1;
        this.loader.hideLoader();

      }, err => {
        this.loader.hideLoader();

      })
    }
    else if (val > 0) {
      this.countermaxpage = this.countermaxpage + 1;
      this.database.getQuotationListDb('all', '', '', this.quotationPerPage, this.next).then((data) => {
        this.listQuotation = data;
        this.dateFormat();
        this.loader.hideLoader();
      }, err => {
        this.loader.hideLoader();

      })
      this.min = false;

      if (this.countermaxpage == this.totalpages) {
        this.min = false;
        this.dateFormat;
        this.max = true;
        console.log("unable")
        console.log(this.min);
        console.log(this.next + "page");
        console.log(this.countermaxpage);

      }

    }
    else if (val < 0) {
      console.log(this.countermaxpage);
      console.log(val < 10);
      this.countermaxpage = this.countermaxpage - 1;
      this.database.getQuotationListDb('all', '', '', this.quotationPerPage, this.next).then((data) => {
        this.listQuotation = data;
        this.dateFormat()
        this.max = false;
        this.loader.hideLoader();
      }, err => {
        this.loader.hideLoader();

      });
    }

  }

  quotationCountFunc() {
    this.database.quotationCount().then((data) => {
      this.quotationCount = data;
      console.log("111111  :" + data);
      if (this.quotationCount <= this.quotationPerPage) {
        this.max = true;
      }
    });
  }


  navToViewReceipts(val) {
    this.navCtrl.push(ReceiptsPage, {
      quotationId: val.id,
      serverId: val.quotation_main_id
    })
  }

  navToAddReceipts(val) {
    console.log(val)
    this.navCtrl.push(AddReceiptsPage, {
      quotationdata: val
    })
  }


  navToEditQuotation(val) {
    this.database.getQuotationById(val.id).then((data) => {
      console.log(" dataaaaaaa");
      console.log(data)
      this.navCtrl.push(QuotationPage, {
        quotationdata: data,

        edit: true
      })
    })

  }


  dateFormat() {
    for (let i = 0; i < this.listQuotation.length; i++) {
      this.listQuotation[i]['date'] = this.convertDate(this.listQuotation[i]['occasionDateTime'].split('T')[0],"-");
      let str:string=this.listQuotation[i]['createdDate']
      if(str.includes('/'))
      {
        this.listQuotation[i]['created_date'] = this.convertDate(this.listQuotation[i]['createdDate'],"/");
      }
      else{
        this.listQuotation[i]['created_date'] = this.convertDate(this.listQuotation[i]['createdDate'].split(' ')[0],"-");
      }
    
    }

  }

}
