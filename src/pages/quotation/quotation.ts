import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { AlertUtil } from '../../providers/alert-util';
import { StandardPage } from '../../pages/standard/standard';
import { CurrentSessionProvider } from '../../providers/current-session/current-session';
import { DatabaseProvider } from '../../providers/database/database';
import { LoaderUtil } from '../../providers/loader-util';

/**
 * Generated class for the QuotationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-quotation',
  templateUrl: 'quotation.html',
  preserveWhitespaces: false

})
export class QuotationPage {
  public quotationForm;
  standardMenu = []
  selectedClass = [true, true, true, true, true, true, true, true, true];
  customerId: string;
  submitAttempt: boolean = false;
  occasionType = ['Birthday', 'Anniversary', 'Marriage', 'Corporate Parties', 'Seminars', 'Others'];
  date: string;
  quotationData = {};
  occasion: string;
  title:string="Add "
  packagesName: number;
  constructor(public loader: LoaderUtil, public database: DatabaseProvider, public currentSession: CurrentSessionProvider, public alert: AlertUtil, public navCtrl: NavController, public navParams: NavParams, public frmbuilder: FormBuilder) {
    console.log("editedddddddddddd    " + this.navParams.get('edit'))
    this.quotationForm = frmbuilder.group({
      name: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      mobile: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      email: [''],
      alternateMobileNo: [''],
      venue: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      occasion: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      occasionDt: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      address: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      packages: ['', Validators.compose([Validators.minLength(0), Validators.required])],

    });
    this.timeFormat()
    this.date = this.dateFormat()+"T"+this.timeFormat()
    console.log("dateeeee")
    console.log(this.date)
    console.log(new Date().toLocaleString())
    console.log(new Date().toLocaleString().split(','))

    this.database.getpackagsDb().then((data) => {
      let packageId = [];
      // console.log(JSON.stringify(data));
      for (let i = 0; i < data.length; i++) {
        // let item = { id: i, name: data[i]['name'],main_id:data[i]['main_id'] }
        this.standardMenu.push(data[i]['name']);
        packageId.push(data[i]['main_id'])
      }

      this.quotationForm.alternateMobileNo = "";
      this.quotationForm.email = "";
      this.quotationForm.occasion = "Birthday"

      console.log(this.standardMenu)

      if (this.navParams.get('edit')) {
        this.quotationData = this.navParams.get('quotationdata');
        console.log("quotation Data")
        console.log(this.quotationData)
        this.title="Edit "
        
        this.quotationForm.name = this.quotationData['customerName'];
        this.quotationForm.mobile = this.quotationData['mobileNo'];
        this.quotationForm.email = this.quotationData['emailId'];
        this.quotationForm.venue = this.quotationData['venue'];
        this.quotationForm.alternateMobileNo = this.quotationData['alternateMobileNo'];
        this.occasion = this.quotationData['occasion'];
        this.date = this.quotationData['occasionDateTime'];
        this.quotationForm.address = this.quotationData['customerAddress'];
        for (let i = 0; i < this.standardMenu.length; i++) {
          console.log("hiii")
          console.log(this.standardMenu[i])

          if (packageId[i] == this.quotationData['main_id']) {
            console.log("enter in i" + i)
            this.packagesName = i

          }
        }
        this.customerId = this.quotationData['customerId']
        // {main_id:this.quotationData['main_id'],name:this.quotationData['packageName']};
        // this.database.getCustomerListDb('single', this.quotationForm.mobile).then((data) => {
        //   if (data.length > 0) {

        //     this.customerId = data[0]['main_id'];
        //   }
        // })
      }

    })
    console.log(this.quotationForm.value.packages)


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuotationPage');
  }



  navToNext() {
    this.submitAttempt = true;
    if (!this.quotationForm.valid) {
      console.log(" error")
    }

    else {
      console.log("current data " + JSON.stringify(this.quotationForm.value))
      // this.currentSession.setCurrentData(this.quotationForm.value.packages)
      let objectQuotation = {};
      objectQuotation = this.quotationForm.value;
      objectQuotation['customerId'] = this.customerId || "";
      console.log("object quotation");
      console.log(JSON.stringify(objectQuotation))
      this.currentSession.setCurrentquotationData(objectQuotation)
      let selectepackage = this.quotationForm.value.packages
      console.log("selected package " + selectepackage)
      this.navCtrl.push(StandardPage, {
        package: this.packagesName,
        quotationData: this.quotationData,
        edit: this.navParams.get('edit')
      })
    }
  }


  timeFormat() {
    let Time = new Date().toLocaleString().split(',')[1];
    let hour:string;
       if(new Date().toLocaleString().split(' ')[2]=='PM'){
         console.log("enter 1")
         hour=(+(new Date().toLocaleString().split(',')[1]).split(":")[0]+12).toString()
       }
       else{   
        console.log("enter 2")
        console.log((Time.split(':')[0]).split(' ')[1])
      console.log(new Date().toLocaleString().split(' ')[2])

         hour=(Time.split(':')[0]).split(' ')[1]
       }
    return  hour+ ":" + Time.split(':')[1]+":46.422Z"
  }

  dateFormat() {
    let dt = new Date().toLocaleString().split(',')[0]
    let date:string; 
    let month:string; 
    if(dt.split('/')[0].length==1){
    date="0"+ dt.split('/')[0]
    }
    else{
      date= dt.split('/')[0]
    }

    if(dt.split('/')[1].length==1){
      month="0"+ dt.split('/')[1]
      }
      else{
        month= dt.split('/')[1]
      }
      
    return dt.split('/')[2] + "-" + month +"-"+ date
  }


  lostFocus(i) {
    // this.selectedClass[i] = true;

  }

  getFocus(i) {
    // this.selectedClass[6] = true;
    // this.selectedClass[7] = true;

    // this.selectedClass[8] = true;

    // this.selectedClass[i] = false;
    console.log(this.quotationForm.value.packages)

  }

  searchCustomer() {

    let mobnumber: string = this.quotationForm.value.mobile;
    console.log("enter1 " + mobnumber)
    if (mobnumber.length == 10) {
      this.loader.showLoader()
      ///"email": String, "createdDate": String, "updatedDate": String, "status": String, "address": String }]
      this.database.getCustomerListDb('single', mobnumber).then((data) => {
        console.log(data);
        if (data.length > 0) {
          this.quotationForm.name = data[0]['name'];
          this.quotationForm.alternateMobileNo = data[0]['alternateMobileNo'];
          this.quotationForm.email = data[0]['email'];
          this.quotationForm.address = data[0]['address'];
          this.customerId = data[0]['main_id'];
        }
        else {
          // this.alert.showOkAlert("Error", "Customer details does not exist.")
        }
        this.loader.hideLoader();
      })
    }

  }

}
