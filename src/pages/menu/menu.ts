import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CurrentSessionProvider } from '../../providers/current-session/current-session';
import { DatabaseProvider } from '../../providers/database/database';
import { LoaderUtil } from '../../providers/loader-util';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
  preserveWhitespaces: false
  
})
export class MenuPage {
  standardMenu = []

  constructor(public loader:LoaderUtil,public database: DatabaseProvider, public currentSession: CurrentSessionProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.loader.showLoader()
    this.database.getcatergoriesItemsDb().then((data) => {
      console.log("db data")
      console.log(JSON.stringify(data));
      this.loader.hideLoader()
      
      this.standardMenu=data
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }




  toggleDetails(val) {
    console.log(val)
    console.log(JSON.stringify(this.standardMenu[val]))
    this.standardMenu[val].showDetails = !this.standardMenu[val].showDetails;

    if (!this.standardMenu[val].showDetails) {
      this.standardMenu[val].icon = 'ios-add-circle-outline';

    } else {
      this.standardMenu[val].icon = 'ios-remove-circle-outline';

    }


  }

  

  

}
