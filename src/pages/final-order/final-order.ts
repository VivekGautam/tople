import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { DatabaseProvider } from '../../providers/database/database';
import { AlertUtil } from '../../providers/alert-util';
import { LoaderUtil } from '../../providers/loader-util';
import { CurrentSessionProvider } from '../../providers/current-session/current-session';
import { ListQuotationPage } from '../../pages/list-quotation/list-quotation';

/**
 * Generated class for the FinalOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-final-order',
  templateUrl: 'final-order.html',
  preserveWhitespaces: false

})
export class FinalOrderPage {
  public finalorder;
  selectedClass = [true, true, true, true, true, true];
  submitAttempt: boolean = false;

  constructor(public currentSession: CurrentSessionProvider, public alert: AlertUtil, public loader: LoaderUtil, public database: DatabaseProvider, public navCtrl: NavController, public navParams: NavParams, public frmbuilder: FormBuilder) {


    this.finalorder = frmbuilder.group({
      comments: ['', Validators.compose([Validators.minLength(1), Validators.required])],
      plates: ['', Validators.compose([Validators.minLength(1), Validators.pattern('[0-9]*'), Validators.required])],
      perPlateAmount: ['', Validators.compose([Validators.minLength(1), Validators.pattern('[0-9]*'), Validators.required])],
      additionalAmount: ['', Validators.compose([Validators.minLength(1), Validators.required])],
      finalAmount: ['', Validators.compose([Validators.minLength(1), Validators.required])],
      total: ['', Validators.compose([Validators.minLength(1), Validators.required])]

    })
    if(this.navParams.get('edit'))
    {
      let quotationData=this.navParams.get('quotationData')
      this.finalorder.comments = quotationData['message'];
      this.finalorder.plates = quotationData['platesNumber'];
      this.finalorder.perPlateAmount = quotationData['plateAmount'];
      this.finalorder.additionalAmount = quotationData['additionalCost'];
      this.finalorder.finalAmount = quotationData['total'];
      this.finalorder.total = +quotationData['platesNumber']*+quotationData['plateAmount'];
      
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinalOrderPage');
  }

  

  save() {
    this.submitAttempt = true;

    if (!this.finalorder.valid) {
      console.log(this.finalorder.value);

    }
    else {
      this.loader.showLoader()

      if(this.navParams.get('edit')){
        console.log("enter in edited")
        let quotationData=this.navParams.get('quotationData')
        this.database.editQuotationDb(this.currentSession.getCurrentQuotationData(), this.currentSession.getSelectedPackage(), this.currentSession.getViewPackageDataItem(), this.finalorder.value,quotationData.id,quotationData.quotation_main_id).then((data) => {
          console.log(data);
          this.loader.hideLoader();
          this.alert.showOkAlert("Successful", "Quotation Saved.");
          this.navCtrl.setRoot(ListQuotationPage)
          this.currentSession.customItem =undefined;
         
          // this.database.syncQuotationApi('quotation').then((data) => {
          //   console.log(data)
            this.database.syncQuotationApi('tempEditquotation').then((data) => {
              console.log(data)
            },
              err => {
                console.log(err);
                this.loader.hideLoader()
    
              })
          // },
          //   err => {
          //     console.log(err);
          //     this.loader.hideLoader()
  
          //   })
        
        
        },
          err => {
            console.log(err);
            this.loader.hideLoader()
  
          })
      }
      else{
        this.database.saveQuotationDb(this.currentSession.getCurrentQuotationData(), this.currentSession.getSelectedPackage(), this.currentSession.getViewPackageDataItem(), this.finalorder.value).then((data) => {
          console.log(data);
          this.loader.hideLoader();
          this.alert.showOkAlert("Successful", "Quotation Saved.");
          this.navCtrl.setRoot(ListQuotationPage)
          this.currentSession.customItem =undefined;
          this.database.syncQuotationApi('quotation').then((data) => {
            console.log(data)
          },
            err => {
              console.log(err);
              this.loader.hideLoader()
  
            })
        },
          err => {
            console.log(err);
            this.loader.hideLoader()
  
          })
      }
    


    }
  }



  lostFocus(i) {
    console.log(i)
    console.log(this.finalorder.value)
    let plates: string = this.finalorder.value.plates;
    let perPlateAmount: string = this.finalorder.value.perPlateAmount;
    let additionalAmount: string = this.finalorder.value.additionalAmount
    if (i == 1 || 2) {
      if (plates!=undefined  && perPlateAmount!=undefined ){
      if (plates.length > 0 && perPlateAmount.length > 0) {
        console.log("enter1 ")
        console.log(+this.finalorder.value.perPlateAmount * +this.finalorder.value.plates)
        let temp: string = '' + (+this.finalorder.value.perPlateAmount * +this.finalorder.value.plates);
        this.finalorder.total = temp
        if (additionalAmount.length > 0) {
          let temp: string = '' + ((+this.finalorder.value.perPlateAmount * +this.finalorder.value.plates) + (+this.finalorder.value.additionalAmount));

          this.finalorder.finalAmount = temp
          console.log("enter2 " + temp)

        }
      }}
    }

    if (i == 5) {
      if (plates!=undefined  && perPlateAmount!=undefined && additionalAmount!=undefined){
      if (plates.length > 0 && perPlateAmount.length > 0 && additionalAmount.length > 0) {
        console.log("enter3 ")
        let temp: string = '' + (+this.finalorder.value.total + (+this.finalorder.value.additionalAmount));

        this.finalorder.finalAmount = temp
      }
    }
    }
  }


  finalAmount() {
    let plates: string = this.finalorder.value.plates;
    let perPlateAmount: string = this.finalorder.value.perPlateAmount;
    if (plates.length > 0 && perPlateAmount.length > 0) {
      let temp: string = '' + (+this.finalorder.value.total + (+this.finalorder.value.additionalAmount));

      this.finalorder.finalAmount = temp

    }
  }
  getFocus(i) {


    // this.selectedClass[i]=false;

  }
}
