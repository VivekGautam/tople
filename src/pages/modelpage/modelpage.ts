import { Component, Renderer } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import { LoaderUtil } from '../../providers/loader-util';
import { DatabaseProvider } from '../../providers/database/database';
import { AlertUtil } from '../../providers/alert-util';
import { CurrentSessionProvider } from '../../providers/current-session/current-session';

/**
 * Generated class for the ModelpagePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-modelpage',
  templateUrl: 'modelpage.html',
  preserveWhitespaces: false

})
export class ModelpagePage {
  selectedItems = [];
  itemsList = [];
  customItem: string;
  custom: boolean = true;
  constructor(public alert: AlertUtil, public currentSession: CurrentSessionProvider, public database: DatabaseProvider, public loader: LoaderUtil, public navCtrl: NavController, public navParams: NavParams,
    public renderer: Renderer,
    public viewCtrl: ViewController) {
      if(this.navParams.get('custom')!=undefined){
        this.custom = this.navParams.get('custom')
        
      }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ModelpagePage');
  }
  dismissModal() {
    this.viewCtrl.dismiss(this.selectedItems).then(() => {
      console.log(" ")
    });
  }

  trackByFn(index, h) {
    return index;
  }



  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.itemsList = [];
      return;
    }
    this.database.getItemsBySearch(val).then((data) => {
      this.itemsList = data;
      console.log("data enter")
      console.log(this.itemsList)

    },
      err => {
        console.log(err)
      })
  }



  addCustomItem() {
    // name: data.rows.item(i).name,
    // itemId: data.rows.item(i).main_id,
    // packageName: ""
    console.log("customer item: "+ this.customItem)
    let customData=this.customItem
    if (this.customItem.length > 0) {
      this.alert.showOkAlert("Save", "Item added to menu");
      this.database.getItemsBySearch(this.customItem).then((data) => {
        let itemsCustom=[];
        itemsCustom=data;
        console.log("data enter")
        console.log(itemsCustom)
        if(itemsCustom.length==1){
          console.log("enter 1")
          this.selectedItems.push({
            name: itemsCustom[0].name, main_id: itemsCustom[0].main_id, categoryId: 0
          })
        }
        else{
          console.log("enter 2")
          console.log("customer data: "+customData)
            this.selectedItems.push({
              name: customData, main_id: 0, categoryId: 0
            })
          
          
        }
  
      },
        err => {
          console.log(err)
        })
     
      this.customItem='';
    }
    else {
      this.alert.showOkAlert("Error", "Please Enter Item Name.");
    }
  }

  // itemListBySearch(data) {
  //   this.searchText = this.searchbar.getValue();

  //   console.log(" selected school")
  //   this.loader.showLoader()
  //   let dt = this.itemsSearch.getResultBySearch(this.searchText)
  //   console.log("data searched");
  //   console.log(dt);

  //   //.then((data) => {
  //   //   this.itemsList = data;
  //   //   this.itemsCount = this.itemsList.length;

  //   //   this.searchByDefault = false;
  //   //   this.loader.hideLoader()
  //   // },
  //   //   err => {
  //   //     console.log(err);
  //   //     this.loader.hideLoader()

  //   //   })


  // }


  itemAdd(item) {
    this.alert.showOkAlert("Save", "Item added to menu");
    this.selectedItems.push(item)
  }
}
