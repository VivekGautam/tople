import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { CurrentSessionProvider } from '../../providers/current-session/current-session';
import { DatabaseProvider } from '../../providers/database/database';
import { LoaderUtil } from '../../providers/loader-util';
/**
 * Generated class for the PackagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-packages',
  templateUrl: 'packages.html',
  preserveWhitespaces: false
  
})
export class PackagesPage {
  standardMenu = []
  
    constructor(public loader:LoaderUtil,public database: DatabaseProvider, public currentSession: CurrentSessionProvider, public navCtrl: NavController, public navParams: NavParams) {
      this.loader.showLoader()
      this.database.getpackagesItemsDb().then((data) => {
        console.log("db data")
        console.log(JSON.stringify(data));
        this.loader.hideLoader()
        
        this.standardMenu=data
      })
    }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackagesPage');
  }


  toggleDetails(val) {
    console.log(val)
    console.log(JSON.stringify(this.standardMenu[val]))
    this.standardMenu[val].showDetails = !this.standardMenu[val].showDetails;

    if (!this.standardMenu[val].showDetails) {
      this.standardMenu[val].icon = 'ios-add-circle-outline';

    } else {
      this.standardMenu[val].icon = 'ios-remove-circle-outline';

    }


  }

}
